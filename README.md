## Re-Identification of Zebrafish using Metric Learning

This repository contains the code and scripts for the paper *Re-Identification of Zebrafish using Metric Learning*

The paper investigates re-identification of zebrafish using color data recorded from a side-view perspective. Five metric and subspace learning methods (KISSME, iKISSME, XQDA, LSSL and DNS) and three baseline methods (l1, l2, and cosine distance) were compared using two feature descriptors (ELF and LOMO) and their color and texture components. The methods are compared using the mean Average Precision (mAP) metric and using 10-fold cross-validation.

As this is a collection of research code, one might find some occasional rough edges. We have tried to clean up the code to a decent level but if you encounter a bug or a regular mistake, please report it in our issue tracker. 

### The AAU Zebrafish Re-Identification Dataset

The evaluation code is built around The AAU Zebrafish Re-Identification dataset which is published on [Kaggle](https://www.kaggle.com/aalborguniversity/aau-zebrafish-reid), recorded in an laboratory environemnt at Aalborg, Denmark.

### Code references

The KISSME code was based on the implementation of [Xiao et al.](https://github.com/Cysu/dgd_person_reid) and the orignal authors [MATLAB code](https://www.tugraz.at/institute/icg/research/team-bischof/lrs/downloads/kissme/).

The XQDA and LOMO code was based on the original authors [MATLAB code](http://www.cbsr.ia.ac.cn/users/scliao/projects/lomo_xqda/).

The Kernelized DNS code was based on the original authors [MATLAB code](https://github.com/lzrobots/NullSpace_ReID).

### License

All code is licensed under the MIT license, except for the aforementioned code reference, which are subject to their respective licenses when applicable.

### Acknowledgements
Please cite the following paper if you use our code or dataset:

```TeX
@InProceedings{Haurum_2020_WACV_Workshops,
author = {Haurum, Joakim Bruslund and Karpova, Anastasija and Pedersen, Malte and Bengtson, Stefan Hein and Moeslund, Thomas B.},
booktitle = {The IEEE Winter Conference on Applications of Computer Vision (WACV) Workshops},
title = {Re-Identification of Zebrafish using Metric Learning},
month = {March},
year = {2020}
} 
```
