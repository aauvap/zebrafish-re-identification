# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund
"""

import numpy as np
import time
import csv
import os
import argparse
import sys
import pickle
from sklearn.metrics.pairwise import pairwise_distances
import matplotlib.pyplot as plt
from evaluation import cmc, plotCurve, mAPk

sys.path.append('../')
from metric_learning.methods import KISSME, LSSL, _calc_PCA


def update_result_dict(dictionary, values, key):
    """
    Updates the supplied dict, by inserting the given values, for the supplied key.
    If the key does not exist in the dict, we create it.
    
    Input:
        dictionary: The dictionary we want to work with
        values: The values which want to add to the values at the key in our dictionary
        key: The key in the dictionary for which we want to adapt the values
    
    Output:
        The updated dictionary
    """
    
    if type(values) is float:
        values = np.asarray(values)
    values = values.reshape(1,-1)
    
    if key in dictionary.keys():
        dictionary[key] = np.concatenate((dictionary[key], values))
    else:
        dictionary[key] = values


def evaluate_features(args):
    """
    Applies several Metric Learning approaches on the supplied features, and evaluates them through mAP and CMC
    
    Currently applies:
        KISSME
        iKISSME
        XQDA
        LSSL
        Linear Kernel Null Space
        RBF Kernel Null Space
        L1 Distance
        L2 Distance 
        Cosine Distance
    
    Input:
        args: A dictionary, holding the fields ['trainFeaturesPath', 'trainLabelsPath', 'testFeaturesPath', 'testLabelsPath', 'rootOutputName', and 'outputPath']
    
    Output:
        Saves a separate .csv and .pkl file with the mAP and CMC results
    """
    np.random.seed(0)
    
    path = args["directoryPath"]
    TRAIN_FEATURE_PATH = os.path.join(path, args["trainFeaturesPath"])
    TRAIN_LABELS_PATH = os.path.join(path, args["trainLabelsPath"])
    FEATURE_NAME = args["rootOutputName"]
    K_DATA_SPLITS = args["kDataSplit"]
    TRAIN_TEST_SPLIT = args["trainTestSplit"]
    MAX_N_TRAIN = args["nTrain"]
    MAX_N_TEST = args["nTest"]
    
    mAP_LIST = False
    OUTPUT_DIR = os.path.join("Results", FEATURE_NAME)
        
    if args["outputPath"] != "":
        OUTPUT_DIR = args["outputPath"]
        
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
        
    assert TRAIN_TEST_SPLIT > 0. and TRAIN_TEST_SPLIT < 1., "The train/test ratio split has to be a value between 0 and 1, you supplied {}".format(TRAIN_TEST_SPLIT) 
    assert type(K_DATA_SPLITS) is int, "The provided kDataSplit variable was not of type int, but {}".format(type(K_DATA_SPLITS))
    assert K_DATA_SPLITS > 0, "The provided number of random data split iterations should be positive, you supplied {}".format(K_DATA_SPLITS)
        
    
    # Load the features and labels
    with open(TRAIN_FEATURE_PATH, "rb") as f:
        dataMat = pickle.load(f)
    
    with open(TRAIN_LABELS_PATH, "rb") as f:
        labels = pickle.load(f)

    label_idx = np.asarray(labels < 3)

    dataMat = dataMat[label_idx]
    labels = labels[label_idx]
        
    # Find the amount of unique labels, and how many time each occurss
    trn_label_count, label_count = np.unique(labels, return_counts = True)
    
    # Enforce each class to only have as many samples used as the amount in the smallest class
    # The samples used are chosen at random
    N = np.min(label_count)    
    
    N_trn = MAX_N_TRAIN
    N_tst = N - N_trn
           

    # Create dicts used to stor the results
    pca_ratios = np.arange(1,101)
    mAP_mean_PCA = np.zeros((9,pca_ratios.shape[0]))
    mAP_std_PCA = np.zeros((9,pca_ratios.shape[0]))
    
    training_time_mean_PCA = np.zeros((6,pca_ratios.shape[0]))
    training_time_std_PCA = np.zeros((6,pca_ratios.shape[0]))
    
    distance_time_mean_PCA = np.zeros((9,pca_ratios.shape[0]))
    distance_time_std_PCA = np.zeros((9,pca_ratios.shape[0]))
    
    evaluation_time_mean_PCA = np.zeros((9,pca_ratios.shape[0]))
    evaluation_time_std_PCA = np.zeros((9,pca_ratios.shape[0]))
    
    total_time_mean_PCA = np.zeros((9,pca_ratios.shape[0]))
    total_time_std_PCA = np.zeros((9,pca_ratios.shape[0]))
    
    pca_labels = None
    
    pca_variances = np.zeros((K_DATA_SPLITS, min(dataMat.shape[1], N_trn*len(trn_label_count))))
    pca_variances_ratio = np.zeros((K_DATA_SPLITS, min(dataMat.shape[1], N_trn*len(trn_label_count))))

    error_lst = []
    
    for pca_idx, pca_ratio in enumerate(pca_ratios):
        CMC_results = {}
        mAP_results = {}
        Train_time = {}
        Distance_time = {}
        Evaluation_time = {}
        print("\nPCA Iteration {} out of {}".format(pca_idx+1, len(pca_ratios)))
        
        
        
        np.random.seed(0)
        for k in range(K_DATA_SPLITS):
            print("\nDataset Iteration {} out of {}".format(k+1, K_DATA_SPLITS))
        
            unique_labels = np.unique(labels)
            N_unique_labels = unique_labels.shape[0]
            
            # Choose N of each fish identity, so that the dataset is balanced
            class_labels = np.empty((unique_labels.shape[0], N))
            for idx in range(unique_labels.shape[0]):
                all_class_labels = np.where(labels == unique_labels[idx])[0]
                class_labels[idx] = all_class_labels[np.random.choice(N,N,replace=False)]
            
            # Setup the vectors holding the labels and indicies for the training and testing data
            trn_inds = class_labels[:,:N_trn]
            tst_inds = class_labels[:,-N_tst:]
            
            labelA_trn_ind = trn_inds[:, :int(N_trn/2)]
            labelB_trn_ind = trn_inds[:, int(N_trn/2):]    
            
            # Reshaped using order F so that instead of the labels being in order, it repeats i.e . instead of 0, 0..., 0, 1, it will be 0,1,0,1
            labelA_trn_ind = labelA_trn_ind.reshape(-1, order = "F").astype(np.int)
            labelB_trn_ind = labelB_trn_ind.reshape(-1, order = "F").astype(np.int)
            Y_trn_ind = trn_inds.reshape(-1, order="F").astype(np.int)
            
            Y_trn = labels[Y_trn_ind].copy()
            
            
            labelA_tst_ind = tst_inds[:, :int(N_tst/2)]
            labelB_tst_ind = tst_inds[:, int(N_tst/2):]
            
            # Reshaped using order F so that instead of the labels being in order, it repeats i.e . instead of 0, 0..., 0, 1, it will be 0,1,0,1
            labelA_tst_ind = labelA_tst_ind.reshape(-1, order = "F").astype(np.int)
            labelB_tst_ind = labelB_tst_ind.reshape(-1, order = "F").astype(np.int)
            Y_tst_ind = tst_inds.reshape(-1, order="F").astype(np.int)
            
            Y_tst = labels[Y_tst_ind].copy() 
            
            dataTrn = dataMat[Y_trn_ind].copy()
            dataTst = dataMat[Y_tst_ind].copy()
        
            print("{} training images per class, {} testing images per class".format(N_trn, N_tst))
            
        
                    
            
            
            
            # Only use a single probe for each class, the rest is used for gallery
            Y_probe = Y_tst[:N_unique_labels]
            Y_gallery = Y_tst[N_unique_labels:]
            
            # PCA from Sklearn return N_trn*2 PCs. 
            # Out of these some amount of the last few ones will have a variance close to 0. 
            # If these are included metric learning results will fall! (Not L1, L2 and Cos. Dist)
            # So if we want 100% variance explained AND good performance,
            # we just use all components untill BUT NOT INCLUDING the components that makes the explained variance 100%


            startT = time.time()
            pca = _calc_PCA(dataTrn, None, pca_ratio/100.)

            if not pca:
                print("SVD DID NOT CONVERGE, SKIPPING THIS FOLD")
                error_lst.append("PCA did not converge: Train {} - Fold {} - PCA Ratio {}".format(N_trn, k, pca_ratio))
                continue

            PCATime = time.time() - startT
            print("PCA learned in {:.4f} seconds".format(PCATime))

            dataPCATrn = pca.transform(dataTrn)
            dataPCATst = pca.transform(dataTst)    
            
            
            dataPCAProbe = dataPCATst[:N_unique_labels]
            dataPCAGallery = dataPCATst[N_unique_labels:] 
            print("Train: Size {} - Shape {} - PCA Shape {}".format(N_trn*N_unique_labels, dataTrn.shape, dataPCATrn.shape))
            print("Test - Total: Size {} - Shape {} - PCA Shape {}".format(N_tst*N_unique_labels, dataTst.shape, dataPCATst.shape))
            print("Test - Probe: Size {} - PCA Shape {}".format(dataPCAProbe.shape[0], dataPCAProbe.shape))
            print("Test - Gallery: Size {} - PCA Shape {}".format(dataPCAGallery.shape[0], dataPCAGallery.shape))
                
            if pca_idx == 0:
                pca_variances[k] = pca.explained_variance_
                pca_variances_ratio[k] = pca.explained_variance_ratio_.cumsum()

                print(pca_variances)
            
            # APPLY METRIC LEARNING 
            
            ## KISSME
            
            kissme = KISSME()
            
            startT = time.time()
            kissme.fit(dataPCATrn, Y_trn, "original", False)
            trainingTime = time.time() - startT
            
            startT = time.time()
            dis_kissme_test = kissme.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_cmc, kissme_rank  = cmc(dis_kissme_test, Y_probe, Y_gallery)
            kissme_mAP, kissme_AP, kissme_id_sorted_indecies = mAPk(dis_kissme_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, kissme_cmc, "KISSME")
            update_result_dict(mAP_results, kissme_mAP, "KISSME")
            update_result_dict(Train_time, trainingTime, "KISSME")
            update_result_dict(Distance_time, distanceTime, "KISSME")
            update_result_dict(Evaluation_time, evalTime, "KISSME")
            
            kissme = KISSME()
            
            startT = time.time()
            kissme.fit(dataPCATrn, Y_trn, "original", True)
            trainingTime = time.time() - startT
            
            startT = time.time()
            dis_kissme_test = kissme.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_cmc, kissme_rank  = cmc(dis_kissme_test, Y_probe, Y_gallery)
            kissme_mAP, kissme_AP, kissme_id_sorted_indecies = mAPk(dis_kissme_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, kissme_cmc, "KISSME-PSD")
            update_result_dict(mAP_results, kissme_mAP, "KISSME-PSD")
            update_result_dict(Train_time, trainingTime, "KISSME-PSD")
            update_result_dict(Distance_time, distanceTime, "KISSME-PSD")
            update_result_dict(Evaluation_time, evalTime, "KISSME-PSD")
            
            
            ## KISSME Improved
            
            kissme_imp = KISSME()
            
            startT = time.time()
            kissme_imp.fit(dataPCATrn, Y_trn, "improved", False)
            trainingTime = time.time() - startT
            
            startT = time.time()
            dis_kissme_imp_test = kissme_imp.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_imp_cmc, kissme_imp_rank  = cmc(dis_kissme_imp_test, Y_probe, Y_gallery)
            kissme_imp_mAP, kissme_imp_AP, kissme_imp_id_sorted_indecies = mAPk(dis_kissme_imp_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, kissme_imp_cmc, "iKISSME")
            update_result_dict(mAP_results, kissme_imp_mAP, "iKISSME")
            update_result_dict(Train_time, trainingTime, "iKISSME")
            update_result_dict(Distance_time, distanceTime, "iKISSME")
            update_result_dict(Evaluation_time, evalTime, "iKISSME")    
            
        
            kissme_imp = KISSME()
            
            startT = time.time()
            kissme_imp.fit(dataPCATrn, Y_trn, "improved", True)
            trainingTime = time.time() - startT
            
            startT = time.time()
            dis_kissme_imp_test = kissme_imp.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            kissme_imp_cmc, kissme_imp_rank  = cmc(dis_kissme_imp_test, Y_probe, Y_gallery)
            kissme_imp_mAP, kissme_imp_AP, kissme_imp_id_sorted_indecies = mAPk(dis_kissme_imp_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, kissme_imp_cmc, "iKISSME-PSD")
            update_result_dict(mAP_results, kissme_imp_mAP, "iKISSME-PSD")
            update_result_dict(Train_time, trainingTime, "iKISSME-PSD")
            update_result_dict(Distance_time, distanceTime, "iKISSME-PSD")
            update_result_dict(Evaluation_time, evalTime, "iKISSME-PSD")
            
            
            
            
            ## LSSL
            
            lssl = LSSL(1.5)
            
            startT = time.time()
            lssl.fit(dataPCATrn, Y_trn, False)
            trainingTime = time.time() - startT
            
            startT = time.time()
            sim_lssl_test = lssl.get_similarity(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            lssl_cmc, lssl_rank  = cmc(sim_lssl_test, Y_probe, Y_gallery, False)
            lssl_mAP, lssl_AP, lssl_id_sorted_indecies = mAPk(sim_lssl_test, Y_probe, Y_gallery, False, return_list = mAP_LIST)
            evalTime = time.time()-startT
                    
            
            update_result_dict(CMC_results, lssl_cmc, "LSSL")
            update_result_dict(mAP_results, lssl_mAP, "LSSL")
            update_result_dict(Train_time, trainingTime, "LSSL")
            update_result_dict(Distance_time, distanceTime, "LSSL")
            update_result_dict(Evaluation_time, evalTime, "LSSL") 
                
            
            lssl = LSSL(1.5)
            
            startT = time.time()
            lssl.fit(dataPCATrn, Y_trn, True)
            trainingTime = time.time() - startT
            
            startT = time.time()
            sim_lssl_test = lssl.get_similarity(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            lssl_cmc, lssl_rank  = cmc(sim_lssl_test, Y_probe, Y_gallery, False)
            lssl_mAP, lssl_AP, lssl_id_sorted_indecies = mAPk(sim_lssl_test, Y_probe, Y_gallery, False, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, lssl_cmc, "LSSL-PSD")
            update_result_dict(mAP_results, lssl_mAP, "LSSL-PSD")
            update_result_dict(Train_time, trainingTime, "LSSL-PSD")
            update_result_dict(Distance_time, distanceTime, "LSSL-PSD")
            update_result_dict(Evaluation_time, evalTime, "LSSL-PSD")
        
            #### L1, L2 and Cosine Distance using PCA
        
        
            startT = time.time()
            dis_l1_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "minkowski", p=1)
            distanceTime = time.time() - startT
            
            startT = time.time()
            l1_cmc, l1_rank  = cmc(dis_l1_test, Y_probe, Y_gallery)
            l1_mAP, l1_AP, l1_id_sorted_indecies  = mAPk(dis_l1_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
                    
            update_result_dict(CMC_results, l1_cmc, "L1-PCA")
            update_result_dict(mAP_results, l1_mAP, "L1-PCA")
            update_result_dict(Distance_time, distanceTime, "L1-PCA")
            update_result_dict(Evaluation_time, evalTime, "L1-PCA") 
                
            
            
            
            startT = time.time()
            dis_l2_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "minkowski", p=2)
            distanceTime = time.time() - startT
            
            startT = time.time()
            l2_cmc, l2_rank  = cmc(dis_l2_test, Y_probe, Y_gallery)
            l2_mAP, l2_AP, l2_id_sorted_indecies = mAPk(dis_l2_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            update_result_dict(CMC_results, l2_cmc, "L2-PCA")
            update_result_dict(mAP_results, l2_mAP, "L2-PCA")
            update_result_dict(Distance_time, distanceTime, "L2-PCA")
            update_result_dict(Evaluation_time, evalTime, "L2-PCA") 
                
            
            
            
            
            ### Cosine distance  (1 - cosine similarty)
            
            startT = time.time()
            dis_cd_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "cosine")
            distanceTime = time.time() - startT
            
            startT = time.time()
            cd_cmc, cd_rank  = cmc(dis_cd_test, Y_probe, Y_gallery)
            cd_mAP, cd_AP, cd_id_sorted_indecies  = mAPk(dis_cd_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time() - startT
            
            update_result_dict(CMC_results, cd_cmc, "Cos. Dist.-PCA")
            update_result_dict(mAP_results, cd_mAP, "Cos. Dist.-PCA")
            update_result_dict(Distance_time, distanceTime, "Cos. Dist.-PCA")
            update_result_dict(Evaluation_time, evalTime, "Cos. Dist.-PCA") 
            
    
    
    
        # Plot CMC and save results
        
        mAP_values = np.asarray(list(mAP_results.values()))
        mAP_mean = np.mean(mAP_values, axis = 1).reshape(-1)
        mAP_std = np.std(mAP_values, axis = 1).reshape(-1)
        
        
        train_values = np.asarray(list(Train_time.values()))
        train_mean = np.mean(train_values, axis = 1).reshape(-1)
        train_std = np.std(train_values, axis = 1).reshape(-1)
        
        
        distance_values = np.asarray(list(Distance_time.values()))
        distance_mean = np.mean(distance_values, axis = 1).reshape(-1)
        distance_std = np.std(distance_values, axis = 1).reshape(-1)
        
        evaluation_values = np.asarray(list(Evaluation_time.values()))
        evaluation_mean = np.mean(evaluation_values, axis = 1).reshape(-1)
        evaluation_std = np.std(evaluation_values, axis = 1).reshape(-1)
        
        total_time_values = np.zeros(evaluation_values.shape)
        total_time_values[:6] += train_values
        total_time_values += distance_values
        total_time_values += evaluation_values
        total_time_mean = np.mean(total_time_values, axis = 1).reshape(-1)
        total_time_std = np.std(total_time_values, axis = 1).reshape(-1)
        
        mAP_mean_PCA[:,pca_idx] = mAP_mean
        mAP_std_PCA[:,pca_idx]  = mAP_std
        
        training_time_mean_PCA[:,pca_idx] = train_mean
        training_time_std_PCA[:,pca_idx] = train_std
        
        distance_time_mean_PCA[:,pca_idx] = distance_mean
        distance_time_std_PCA[:,pca_idx] = distance_std
        
        evaluation_time_mean_PCA[:,pca_idx] = evaluation_mean
        evaluation_time_std_PCA[:,pca_idx] = evaluation_std
        
        total_time_mean_PCA[:,pca_idx] = total_time_mean
        total_time_std_PCA[:,pca_idx] = total_time_std
    
        
        pca_labels = list(mAP_results.keys())
        print(total_time_std_PCA.shape)
        del mAP_values
    
    
    plt.figure(1,(15,10))
    plt.cm.tab20
    plt.subplot(2,2,1)
    for plot_idx in range(total_time_mean_PCA.shape[0]):
        line = "-"
        if pca_labels[plot_idx] in ["L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
            line = "--"
        plt.plot(pca_ratios, total_time_mean_PCA[plot_idx], label=pca_labels[plot_idx], linestyle = line)
    plt.title('Total Time')
    plt.ylabel('Time (s)')
    plt.xlim(0, 100)
    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_xticks(np.arange(0,101,10))
    plt.gca().set_xticks(np.arange(0,101,5),minor=True)
    
    
    plt.subplot(2,2,2)
    for plot_idx in range(training_time_mean_PCA.shape[0]):
        line = "-"
        if pca_labels[plot_idx] in ["L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
            line = "--"
        plt.plot(pca_ratios, training_time_mean_PCA[plot_idx],  label=pca_labels[plot_idx], linestyle = line)
    plt.title("Training Time")
    plt.xlim(0, 100)
    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_xticks(np.arange(0,101,10))
    plt.gca().set_xticks(np.arange(0,101,5),minor=True)
    
    
    plt.subplot(2,2,3)
    for plot_idx in range(distance_time_mean_PCA.shape[0]):
        line = "-"
        if pca_labels[plot_idx] in ["L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
            line = "--"
        plt.plot(pca_ratios, distance_time_mean_PCA[plot_idx], label=pca_labels[plot_idx], linestyle = line)
    plt.title('Comparison Time')
    plt.ylabel('Time (s)')
    plt.xlabel('Explained variance included (%)')
    plt.xlim(0, 100)
    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_xticks(np.arange(0,101,10))
    plt.gca().set_xticks(np.arange(0,101,5),minor=True)
    
    
    plt.subplot(2,2,4)
    for plot_idx in range(evaluation_time_mean_PCA.shape[0]):
        line = "-"
        if pca_labels[plot_idx] in ["L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
            line = "--"
        plt.plot(pca_ratios, evaluation_time_mean_PCA[plot_idx],  label=pca_labels[plot_idx], linestyle = line)
    plt.title('Evaluation time')
    plt.xlim(0, 100)
    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_xticks(np.arange(0,101,10))
    plt.gca().set_xticks(np.arange(0,101,5),minor=True)
    
    
    
    plt.legend(bbox_to_anchor=(-1.15,-0.1, 2., -.052), loc=9,
           ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel('Explained variance included (%)')
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_TIME.pdf"), bbox_inches="tight")
    
    
    
    plt.figure(2,(15,10))
    for plot_idx in range(mAP_mean_PCA.shape[0]):
        line = "-"
        if pca_labels[plot_idx] in ["L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
            line = "--"
        plt.plot(pca_ratios, mAP_mean_PCA[plot_idx] * 100, label=pca_labels[plot_idx], linestyle=line)
        

    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_xticks(np.arange(0,101,10))
    plt.gca().set_xticks(np.arange(0,101,5),minor=True)
    plt.gca().set_yticks(np.arange(0,101,10))
    plt.gca().set_yticks(np.arange(0,101,5),minor=True)
    
    
    plt.legend(bbox_to_anchor=(0.,-0.025, 1., -.052), loc=9,
           ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel('Explained variance included (%)')
    plt.ylabel('mAP (%)')
    plt.ylim(0, 105)
    plt.xlim(0, 100)
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_mAP.pdf"), bbox_inches="tight")
    
    
    
    
    plt.figure(3,(15,10))
    plt.clf()
    for idx in range(pca_variances.shape[0]):
        plt.plot(np.arange(len(pca_variances[idx])),pca_variances[idx],label= str(idx+1))

    plt.grid()
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.xlabel('Component count')
    plt.ylabel('Eigenvalue')
    plt.xlim(1, pca_variances.shape[1])
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_EIG_SPLIT.pdf"), bbox_inches="tight")
        
      
    
    plt.figure(4,(15,10))
    plt.clf()
    for idx in range(pca_variances_ratio.shape[0]):
        plt.plot(np.arange(len(pca_variances_ratio[idx])),pca_variances_ratio[idx]*100,label= str(idx+1))

    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_yticks(np.arange(0,101,10))
    plt.gca().set_yticks(np.arange(0,101,5),minor=True)
    
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.xlabel('Component count')
    plt.ylabel('Explained Variance (%)')
    plt.ylim(0, 105)
    plt.xlim(1, pca_variances.shape[1])
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_VAR_SPLIT.pdf"), bbox_inches="tight")
    
    full_variance = None
    full_variance_ratio = None
    pca = _calc_PCA(dataMat)
    full_variance = pca.explained_variance_
    full_variance_ratio = pca.explained_variance_ratio_.cumsum()
    plt.figure(5,(15,10))
    plt.clf()
    plt.plot(np.arange(len(full_variance)),full_variance)
    plt.grid()
    plt.xlabel('Component count')
    plt.ylabel('Eigenvalue')
    plt.xlim(1, len(full_variance))
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_EIG_FULL.pdf"), bbox_inches="tight")
        
    plt.figure(6,(15,10))
    plt.clf()
    plt.plot(np.arange(len(full_variance_ratio)),full_variance_ratio*100)
    
    plt.grid(which="minor",alpha=0.5)
    plt.grid(which="major",alpha=1.0)
    plt.gca().set_yticks(np.arange(0,101,10))
    plt.gca().set_yticks(np.arange(0,101,5),minor=True)

    plt.xlabel('Component count')
    plt.ylabel('Explained Variance (%)')
    plt.xlim(1, len(full_variance_ratio))
    plt.ylim(0, 105)
    plt.savefig(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_VAR_FULL.pdf"), bbox_inches="tight")
    
        
    
    
    
    
    mAP_PKL = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_mAP.pkl")
    TIME_PKL = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_EF_TIME.pkl")
    VAR_PKL = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_VAR.pkl")
    SETTINGS_TXT = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_PCA_EF_SETTINGS.txt")

            
    with open(mAP_PKL, "wb") as f:
        mAP = {}
        mAP["mean"] = mAP_mean_PCA
        mAP["std"] = mAP_std_PCA
        mAP["labels"] = pca_labels
        mAP["All"] = mAP_results
        pickle.dump(mAP, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(TIME_PKL, "wb") as f:
        time_data = {}
        time_data["Total Time mean"] = total_time_mean_PCA
        time_data["Total Time std"] = total_time_std_PCA
        time_data["Total Time"] = total_time_values
        
        time_data["Training Time mean"] = training_time_mean_PCA
        time_data["Training Time std"] = training_time_std_PCA
        time_data["Training Time"] = Train_time
        
        time_data["Distance Time mean"] = distance_time_mean_PCA
        time_data["Distance Time std"] = distance_time_std_PCA
        time_data["Distance Time"] = Distance_time
        
        time_data["Evaluation Time mean"] = evaluation_time_mean_PCA
        time_data["Evaluation Time std"] = evaluation_time_std_PCA
        time_data["Evaluation Time"] = Evaluation_time
        
        time_data["labels"] = pca_labels
        
        pickle.dump(time_data, f, protocol = pickle.HIGHEST_PROTOCOL)
        

    with open(VAR_PKL, "wb") as f:
        pca_data = {}
        pca_data["Full Dataset Variance"] = full_variance
        pca_data["Split Dataset Variances"] = pca_variances
        pca_data["Full Dataset Variance Ratio"] = full_variance_ratio
        pca_data["Split Dataset Variances Ratio"] = pca_variances_ratio
        pickle.dump(pca_data, f, protocol = pickle.HIGHEST_PROTOCOL)
    
    with open(SETTINGS_TXT, "w") as text_file:
        for key in args.keys():
            text_file.write(key + ": " + str(args[key]) + "\n")

    with open(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_ERRORS.txt"), "w") as text_file:
        text_file.write("\n".join(error_lst))



if __name__ == "__main__":
    
    ap = argparse.ArgumentParser(
        description = "Applies the different metric learning approaches on the supplied features")
    ap.add_argument("-directoryPath", "--directoryPath", type=str, default = "",
                help="Path to the directory with all the pkl files")
    ap.add_argument("-trainFeaturesPath", "--trainFeaturesPath", type=str, default = "2F_2L_TRN6_M_Rotated_both_Flipped_ELF.pkl",
                help="Path to the training features that need to be analyzed")
    ap.add_argument("-trainLabelsPath", "--trainLabelsPath", type=str, default = "2F_2L_TRN6_M_Rotated_both_Flipped_LABELS.pkl",
                help="Path to the labels of the training features that need to be analyzed")
    ap.add_argument("-outputPath", "--outputPath", type=str, default = "",
                help="Path to output folder. If provided the .csv file will be saved at the specified location. Else it will be saved where this script is placed")
    ap.add_argument("-rootOutputName", "--rootOutputName", type=str, default = "Test",
                help="Root name of the to output files")
    ap.add_argument("-kDataSplit", "--kDataSplit", type=int, default = 10,
                help="Number of random data splits tested")
    ap.add_argument("-trainTestSplit", "--trainTestSplit", type=float, default = 0.5,
                help="Ratio of training and testing dataset, when not having a specific testing dataset")
    ap.add_argument("-nTrain", "--nTrain", type=int, default = None,
                help="Amount of images used per identity for training set")
    ap.add_argument("-nTest", "--nTest", type=int, default = 2,
                help="Amount of images used per identity for testing set")
    
    
    
    args = vars(ap.parse_args())
    evaluate_features(args)
    