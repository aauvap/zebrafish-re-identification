# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund
"""

import numpy as np
import time
import csv
import os
import argparse
import pickle
import sys
from sklearn.metrics.pairwise import pairwise_distances
from evaluation import cmc, plotCurve, mAPk

sys.path.append('../')
from metric_learning.methods import KISSME, XQDA, LSSL, NullSpace, RBF_kernel, _calc_PCA, Linear_kernel


def find_incorrect_ID_frames(Yp, Yg, indecies, filenames, counter = None):
    """
    Finds the filename of the IDS which were assigned a higher rank than they should have been per probe, and saves it into a dictionary
    
    Input:
        Yp: Labels of the probe features
        Yg: Labels of the gallery features
        indecies: A matrix of sorted indecies, of size [n_probe, n_gallery]
        filenames: A list of filenames where the gallery features were extracted from
        counter: An optional int parameter, which will be added to dictionary key
        
    Output:
        idx_Dict: A dictionary where each key (one per probe) contsists of a list of tuple, containing the filename, corret ID and rank
    """
    
    idx_Dict = {}
    for probe_idx in range(len(Yp)):
        # Create a binary array, indicating whether the probe and gallery images belong to the same class
        ranked_list = Yp[probe_idx] == Yg[indecies[probe_idx]]
        label_count = np.sum(ranked_list)
        
        # Check if all of the occurance of the fish belonging to the current probe is in the start
        # If they are, all were correctly assigned and we got a mAP of 100%
        # IF not some other incorrect fish has been ranked higher than they should. We then find these
        if np.sum(ranked_list[:label_count]) < label_count:
            incorrect_id_lst = []
            
            #F Find the index of the last correctly assigned label. No reason to investigate afterwards
            last_index = np.max(np.where(ranked_list)[0])
            
            for idx in range(last_index):
                if not ranked_list[idx]:
                    # Saves the filename, ID of the incorrectly assigned fish and the rank it got
                    incorrect_id_lst.append((filenames[indecies[probe_idx, idx]], Yg[indecies[probe_idx, idx]], idx))
                    
            # Save all the incorrect assignments in a single dict belonging to the label
            key = "Probe"+str(Yp[probe_idx])
            if counter is not None and type(counter) == int:
                key += "-" +str(counter)
            idx_Dict[key] = incorrect_id_lst
        
    return idx_Dict


def update_result_dict(dictionary, values, key):
    """
    Updates the supplied dict, by inserting the given values, for the supplied key.
    If the key does not exist in the dict, we create it.
    
    Input:
        dictionary: The dictionary we want to work with
        values: The values which want to add to the values at the key in our dictionary
        key: The key in the dictionary for which we want to adapt the values
    
    Output:
        The updated dictionary
    """
    
    if type(values) is float:
        values = np.asarray(values)
    values = values.reshape(1,-1)
    
    if key in dictionary.keys():
        dictionary[key] = np.concatenate((dictionary[key], values))
    else:
        dictionary[key] = values
        

def update_ID_dict(dictionary, values, key):
    """
    Updates the incorrect ID dict, by the given values, for the supplied key.
    If the key does not exist in the dict, we create it.
    
    Input:
        dictionary: The dictionary we want to work with
        values: The values which want to add to the values at the key in our dictionary
        key: The key in the dictionary for which we want to adapt the values
    
    Output:
        The updated dictionary
    """
    
    if key in dictionary.keys():
        dictionary[key].update(values)
    else:
        dictionary[key] = values



def evaluate_features(args):
    """
    Applies several Metric Learning approaches on the supplied features, and evaluates them through mAP and CMC
    
    Currently applies:
        KISSME
        iKISSME
        XQDA
        LSSL
        Linear Kernel Null Space
        RBF Kernel Null Space
        L1 Distance
        L2 Distance 
        Cosine Distance
    
    Input:
        args: A dictionary, holding the fields ['trainFeaturesPath', 'trainLabelsPath', 'testFeaturesPath', 'testLabelsPath', 'rootOutputName', and 'outputPath']
    
    Output:
        Saves a separate .csv and .pkl file with the mAP and CMC results
    """
    np.random.seed(1)
    
    path = args["directoryPath"]
    TRAIN_FEATURE_PATH = os.path.join(path, args["trainFeaturesPath"])
    TRAIN_LABELS_PATH = os.path.join(path, args["trainLabelsPath"])
    TRAIN_FILES_PATH = os.path.join(path, args["trainFilesPath"])
    FEATURE_NAME = args["rootOutputName"]
    K_DATA_SPLITS = args["kDataSplit"]
    TRAIN_TEST_SPLIT = args["trainTestSplit"]
    PCA_RATIO = args["pcaRatio"]
    MAX_N_TRAIN = args["nTrain"]
    MAX_N_TEST = args["nTest"]
    
    mAP_LIST = True
    OUTPUT_DIR = os.path.join("Results", FEATURE_NAME)

    if args["outputPath"] != "":
        OUTPUT_DIR = args["outputPath"]
        
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
        
    assert TRAIN_TEST_SPLIT > 0. and TRAIN_TEST_SPLIT < 1., "The train/test ratio split has to be a value between 0 and 1, you supplied {}".format(TRAIN_TEST_SPLIT)
    assert type(K_DATA_SPLITS) is int, "The provided kDataSplit variable was not of type int, but {}".format(type(K_DATA_SPLITS))
    assert K_DATA_SPLITS > 0, "The provided number of random data split iterations should be positive, you supplied {}".format(K_DATA_SPLITS)
        

    # Load the features and labels
    with open(TRAIN_FEATURE_PATH, "rb") as f:
        dataMat = pickle.load(f)
    
    with open(TRAIN_LABELS_PATH, "rb") as f:
        labels = pickle.load(f)
        
    with open(TRAIN_FILES_PATH, "rb") as f:
        filenames_total = np.asarray(pickle.load(f))
        
    # Find the amount of unique labels, and how many time each occurss
    _, label_count = np.unique(labels, return_counts = True)
    
    # Enforce each class to only have as many samples used as the amount in the smallest class
    # The samples used are chosen at random
    N = np.min(label_count)    
    N_trn = MAX_N_TRAIN
    N_tst = N - N_trn
       
        
    train_range = list(range(2,N_trn+1))

    across_trn_mean = {"KISSME": [],
                  "KISSME-PSD": [],
                  "iKISSME": [],
                  "iKISSME-PSD": [],
                  "LSSL": [],
                  "LSSL-PSD": [],
                  "XQDA": [],
                  "XQDA-PSD":[],
                  "DNS-Linear": [],
                  "DNS-RBF":[],
                  "L1": [],
                  "L1-PCA": [],
                  "L2":[],
                  "L2-PCA":[],
                  "Cos. Dist.": [],
                  "Cos. Dist.-PCA": []}

    across_trn_std = {"KISSME": [],
                  "KISSME-PSD": [],
                  "iKISSME": [],
                  "iKISSME-PSD": [],
                  "LSSL": [],
                  "LSSL-PSD": [],
                  "XQDA": [],
                  "XQDA-PSD":[],
                  "DNS-Linear": [],
                  "DNS-RBF":[],
                  "L1": [],
                  "L1-PCA": [],
                  "L2":[],
                  "L2-PCA":[],
                  "Cos. Dist.": [],
                  "Cos. Dist.-PCA": []}

    error_lst = []

    for train_amount in train_range:

        N_trn = train_amount
            
        # Create dicts used to stor the results
        CMC_results = {}
        mAP_results = {}
        incorrect_frames = {} 
        Train_time = {}
        Distance_time = {}
        Evaluation_time = {}

        np.random.seed(1)
        for k in range(K_DATA_SPLITS):
            print("\nIteration {} out of {}".format(k+1, K_DATA_SPLITS))
        
            unique_labels = np.unique(labels)
            N_unique_labels = unique_labels.shape[0]
            
            # Choose N of each fish identity, so that the dataset is balanced
            class_labels = np.empty((unique_labels.shape[0], N))
            for idx in range(unique_labels.shape[0]):
                all_class_labels = np.where(labels == unique_labels[idx])[0]
                class_labels[idx] = all_class_labels[np.random.choice(N,N,replace=False)]
            
            # Setup the vectors holding the labels and indicies for the training and testing data
            trn_inds = class_labels[:,:N_trn]
            tst_inds = class_labels[:,-N_tst:]
            
            labelA_trn_ind = trn_inds[:, :int(N_trn/2)]
            labelB_trn_ind = trn_inds[:, int(N_trn/2):]    
            
            # Reshaped using order F so that instead of the labels being in order, it repeats i.e . instead of 0, 0..., 0, 1, it will be 0,1,0,1
            labelA_trn_ind = labelA_trn_ind.reshape(-1, order = "F").astype(np.int)
            labelB_trn_ind = labelB_trn_ind.reshape(-1, order = "F").astype(np.int)
            Y_trn_ind = trn_inds.reshape(-1, order="F").astype(np.int)
            
            Y_trn = labels[Y_trn_ind].copy()
            
            
            labelA_tst_ind = tst_inds[:, :int(N_tst/2)]
            labelB_tst_ind = tst_inds[:, int(N_tst/2):]
            
            # Reshaped using order F so that instead of the labels being in order, it repeats i.e . instead of 0, 0..., 0, 1, it will be 0,1,0,1
            labelA_tst_ind = labelA_tst_ind.reshape(-1, order = "F").astype(np.int)
            labelB_tst_ind = labelB_tst_ind.reshape(-1, order = "F").astype(np.int)
            Y_tst_ind = tst_inds.reshape(-1, order="F").astype(np.int)
            
            Y_tst = labels[Y_tst_ind].copy()
            filenames = filenames_total[Y_tst_ind].copy() 
        
            dataTrn = dataMat[Y_trn_ind].copy()
            dataTst = dataMat[Y_tst_ind].copy()
            
            print("{} training images per class, {} testing images per class, {} unique classes".format(N_trn, N_tst, N_unique_labels))
                
            
            
            
            
            # Only use a single probe for each class, the rest is used for gallery
            Y_probe = Y_tst[:N_unique_labels]
            Y_gallery = Y_tst[N_unique_labels:]
            gallery_filenames = filenames[N_unique_labels:]
            
            # PCA from Sklearn return N_trn*2 PCs. 
            # Out of these some amount of the last few ones will have a variance close to 0. 
            # If these are included metric learning results will fall! (Not L1, L2 and Cos. Dist)
            # So if we want 100% variance explained AND good performance,
            # we just use all components untill BUT NOT INCLUDING the components that makes the explained variance 100%

            startT = time.time()
            pca = _calc_PCA(dataTrn, None, PCA_RATIO)

            if not pca:
                print("SVD DID NOT CONVERGE, SKIPPING THIS FOLD")
                error_lst.append("PCA did not converge: Train {} - Fold {}".format(train_amount, k))
                continue

            PCATime = time.time() - startT
            print("PCA learned in {:.4f} seconds".format(PCATime))
            
            dataPCATrn = pca.transform(dataTrn)
            dataPCATst = pca.transform(dataTst)    

              
            dataProbe = dataTst[:N_unique_labels]
            dataGallery = dataTst[N_unique_labels:]
            dataPCAProbe = dataPCATst[:N_unique_labels]
            dataPCAGallery = dataPCATst[N_unique_labels:]   
            print("Train: Size {} - Shape {} - PCA Shape {}".format(N_trn*N_unique_labels, dataTrn.shape, dataPCATrn.shape))
            print("Test - Total: Size {} - Shape {} - PCA Shape {}".format(N_tst*N_unique_labels, dataTst.shape, dataPCATst.shape))
            print("Test - Probe: Size {} - Shape {} - PCA Shape {}".format(dataProbe.shape[0], dataProbe.shape, dataPCAProbe.shape))
            print("Test - Gallery: Size {} - Shape {} - PCA Shape {}".format(dataGallery.shape[0], dataGallery.shape, dataPCAGallery.shape))
            print()
            
            # APPLY METRIC LEARNING 
            
            ## KISSME
            
            kissme = KISSME()
            
            startT = time.time()
            kissme.fit(dataPCATrn, Y_trn, "original", False)
            trainingTime = time.time() - startT
            print("Learned original KISSME in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            dis_kissme_test = kissme.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_cmc, kissme_rank  = cmc(dis_kissme_test, Y_probe, Y_gallery)
            kissme_mAP, kissme_AP, kissme_id_sorted_indecies = mAPk(dis_kissme_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            kissme_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, kissme_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, kissme_cmc, "KISSME")
            update_result_dict(mAP_results, kissme_mAP, "KISSME")
            update_result_dict(Train_time, trainingTime, "KISSME")
            update_result_dict(Distance_time, distanceTime, "KISSME")
            update_result_dict(Evaluation_time, evalTime, "KISSME")
            update_ID_dict(incorrect_frames, kissme_ids, "KISSME")          
            
            
            kissme = KISSME()
            
            startT = time.time()
            kissme.fit(dataPCATrn, Y_trn, "original", True)
            trainingTime = time.time() - startT
            print("Learned original KISSME-PSD in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            dis_kissme_test = kissme.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_cmc, kissme_rank  = cmc(dis_kissme_test, Y_probe, Y_gallery)
            kissme_mAP, kissme_AP, kissme_id_sorted_indecies = mAPk(dis_kissme_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            kissme_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, kissme_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, kissme_cmc, "KISSME-PSD")
            update_result_dict(mAP_results, kissme_mAP, "KISSME-PSD")
            update_ID_dict(incorrect_frames, kissme_ids, "KISSME-PSD")          
            update_result_dict(Train_time, trainingTime, "KISSME-PSD")
            update_result_dict(Distance_time, distanceTime, "KISSME-PSD")
            update_result_dict(Evaluation_time, evalTime, "KISSME-PSD")
            
            
            ## KISSME Improved
            
            kissme_imp = KISSME()
            
            startT = time.time()
            kissme_imp.fit(dataPCATrn, Y_trn, "improved", False)
            trainingTime = time.time() - startT
            print("Learned improved KISSME in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            dis_kissme_imp_test = kissme_imp.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()
            kissme_imp_cmc, kissme_imp_rank  = cmc(dis_kissme_imp_test, Y_probe, Y_gallery)
            kissme_imp_mAP, kissme_imp_AP, kissme_imp_id_sorted_indecies = mAPk(dis_kissme_imp_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            kissme_imp_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, kissme_imp_id_sorted_indecies, gallery_filenames, k)
        
            update_result_dict(CMC_results, kissme_imp_cmc, "iKISSME")
            update_result_dict(mAP_results, kissme_imp_mAP, "iKISSME")
            update_ID_dict(incorrect_frames, kissme_imp_ids, "iKISSME")            
            update_result_dict(Train_time, trainingTime, "iKISSME")
            update_result_dict(Distance_time, distanceTime, "iKISSME")
            update_result_dict(Evaluation_time, evalTime, "iKISSME")    
            
        
            kissme_imp = KISSME()
            
            startT = time.time()
            kissme_imp.fit(dataPCATrn, Y_trn, "improved", True)
            trainingTime = time.time() - startT
            print("Learned improved KISSME-PSD in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            dis_kissme_imp_test = kissme_imp.get_distance(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            kissme_imp_cmc, kissme_imp_rank  = cmc(dis_kissme_imp_test, Y_probe, Y_gallery)
            kissme_imp_mAP, kissme_imp_AP, kissme_imp_id_sorted_indecies = mAPk(dis_kissme_imp_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            kissme_imp_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, kissme_imp_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, kissme_imp_cmc, "iKISSME-PSD")
            update_result_dict(mAP_results, kissme_imp_mAP, "iKISSME-PSD")
            update_ID_dict(incorrect_frames, kissme_imp_ids, "iKISSME-PSD")      
            update_result_dict(Train_time, trainingTime, "iKISSME-PSD")
            update_result_dict(Distance_time, distanceTime, "iKISSME-PSD")
            update_result_dict(Evaluation_time, evalTime, "iKISSME-PSD")
            
            
            
            
            ## LSSL
            
            lssl = LSSL(1.5)
            
            startT = time.time()
            lssl.fit(dataPCATrn, Y_trn, False)
            trainingTime = time.time() - startT
            print("Learned LSSL in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            sim_lssl_test = lssl.get_similarity(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            lssl_cmc, lssl_rank  = cmc(sim_lssl_test, Y_probe, Y_gallery, False)
            lssl_mAP, lssl_AP, lssl_id_sorted_indecies = mAPk(sim_lssl_test, Y_probe, Y_gallery, False, return_list = mAP_LIST)
            evalTime = time.time()-startT
                    
            lssl_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, lssl_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, lssl_cmc, "LSSL")
            update_result_dict(mAP_results, lssl_mAP, "LSSL")
            update_ID_dict(incorrect_frames, lssl_ids, "LSSL")      
            update_result_dict(Train_time, trainingTime, "LSSL")
            update_result_dict(Distance_time, distanceTime, "LSSL")
            update_result_dict(Evaluation_time, evalTime, "LSSL") 
                
            
            lssl = LSSL(1.5)
            
            startT = time.time()
            lssl.fit(dataPCATrn, Y_trn, True)
            trainingTime = time.time() - startT
            print("Learned LSSL-PSD in {:.4f} seconds".format(trainingTime))
            
            startT = time.time()
            sim_lssl_test = lssl.get_similarity(dataPCAProbe, dataPCAGallery)
            distanceTime = time.time() - startT
            
            startT = time.time()        
            lssl_cmc, lssl_rank  = cmc(sim_lssl_test, Y_probe, Y_gallery, False)
            lssl_mAP, lssl_AP, lssl_id_sorted_indecies = mAPk(sim_lssl_test, Y_probe, Y_gallery, False, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            lssl_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, lssl_id_sorted_indecies, gallery_filenames, k)

            update_result_dict(CMC_results, lssl_cmc, "LSSL-PSD")
            update_result_dict(mAP_results, lssl_mAP, "LSSL-PSD")
            update_ID_dict(incorrect_frames, lssl_ids, "LSSL-PSD")       
            update_result_dict(Train_time, trainingTime, "LSSL-PSD")
            update_result_dict(Distance_time, distanceTime, "LSSL-PSD")
            update_result_dict(Evaluation_time, evalTime, "LSSL-PSD")
                
            
            ## XQDA
            
            xqda = XQDA()
            startT = time.time()
            xqda.fit(dataTrn, Y_trn, psd = False)
            trainingTime = time.time() - startT
            print("Learned XQDA in {:.4f} seconds".format(trainingTime))
            if xqda.SVD_completed:
            
                startT = time.time()
                dis_xqda_test = xqda.get_distance(dataProbe, dataGallery)
                distanceTime = time.time() - startT
                
                startT = time.time()        
                xqda_cmc, xqda_rank  = cmc(dis_xqda_test, Y_probe, Y_gallery)
                xqda_mAP, xqda_AP, xqda_id_sorted_indecies  = mAPk(dis_xqda_test, Y_probe, Y_gallery, return_list = mAP_LIST)
                evalTime = time.time()-startT
                        
                xqda_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, xqda_id_sorted_indecies, gallery_filenames, k)
            else:
                error_lst.append("XQDA did not converge: Train {} - Fold {}".format(train_amount, k))
                distanceTime = -1.0
                evalTime = -1.0
                xqda_cmc = np.asarray([np.nan]*dataGallery.shape[0])
                xqda_mAP = np.asarray([np.nan]*dataGallery.shape[0])
                xqda_ids = {}
        
            update_result_dict(CMC_results, xqda_cmc, "XQDA")
            update_result_dict(mAP_results, xqda_mAP, "XQDA")
            update_ID_dict(incorrect_frames, xqda_ids, "XQDA")     
            update_result_dict(Train_time, trainingTime, "XQDA")
            update_result_dict(Distance_time, distanceTime, "XQDA")
            update_result_dict(Evaluation_time, evalTime, "XQDA") 
            
            
            
            xqda = XQDA()
            startT = time.time()
            xqda.fit(dataTrn, Y_trn, psd = True)
            trainingTime = time.time() - startT
            print("Learned XQDA-PSD in {:.4f} seconds".format(trainingTime))
            
            if xqda.SVD_completed: 
                startT = time.time()
                dis_xqda_test = xqda.get_distance(dataProbe, dataGallery)
                distanceTime = time.time() - startT
                
                startT = time.time()        
                xqda_cmc, xqda_rank  = cmc(dis_xqda_test, Y_probe, Y_gallery)
                xqda_mAP, xqda_AP, xqda_id_sorted_indecies  = mAPk(dis_xqda_test, Y_probe, Y_gallery, return_list = mAP_LIST)
                evalTime = time.time()-startT
                        
                xqda_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, xqda_id_sorted_indecies, gallery_filenames, k)

            else:
                error_lst.append("XQDA-PSD did not converge: Train {} - Fold {}".format(train_amount, k))
                distanceTime = -1.0
                evalTime = -1.0
                xqda_cmc = np.asarray([np.nan]*dataGallery.shape[0])
                xqda_mAP = np.asarray([np.nan]*dataGallery.shape[0])
                xqda_ids = {}

            update_result_dict(CMC_results, xqda_cmc, "XQDA-PSD")
            update_result_dict(mAP_results, xqda_mAP, "XQDA-PSD")
            update_ID_dict(incorrect_frames, xqda_ids, "XQDA-PSD")     
            update_result_dict(Train_time, trainingTime, "XQDA-PSD")
            update_result_dict(Distance_time, distanceTime, "XQDA-PSD")
            update_result_dict(Evaluation_time, evalTime, "XQDA-PSD") 
            
            
            
            # Null Space - Kernel
            ## Linear kernel
            
            null_linear_kernel = NullSpace(True)
            
            startT = time.time()
            K_trn = Linear_kernel(dataTrn)
            null_linear_kernel.fit(K_trn,Y_trn)
            trainingTime = time.time() - startT
            print("Learned Linear Kernalized Null Space in {:.4f} seconds".format(trainingTime))
            if null_linear_kernel.SVD_completed: 
            
                startT = time.time()
                K_probe = Linear_kernel(dataTrn, dataProbe)
                K_gallery = Linear_kernel(dataTrn, dataGallery)
                dis_null_linear_kernel_test = null_linear_kernel.get_distance(K_probe, K_gallery)
                distanceTime = time.time() - startT

                startT = time.time()     
                null_linear_kernel_cmc, null_linear_kernel_rank  = cmc(dis_null_linear_kernel_test, Y_probe , Y_gallery)
                null_linear_kernel_mAP, null_linear_kernel_AP, null_linear_id_sorted_indecies  = mAPk(dis_null_linear_kernel_test, Y_probe , Y_gallery, return_list = mAP_LIST)
                evalTime = time.time()-startT
                
                null_linear_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, null_linear_id_sorted_indecies, gallery_filenames, k)
            else:
                error_lst.append("DNS-Linear did not converge: Train {} - Fold {}".format(train_amount, k))
                distanceTime = -1.0
                evalTime = -1.0
                null_linear_kernel_cmc = np.asarray([np.nan]*dataGallery.shape[0])
                null_linear_kernel_mAP = np.asarray([np.nan]*dataGallery.shape[0])
                null_linear_ids = {}
            
            update_result_dict(CMC_results, null_linear_kernel_cmc, "DNS-Linear")
            update_result_dict(mAP_results, null_linear_kernel_mAP, "DNS-Linear")
            update_ID_dict(incorrect_frames, null_linear_ids, "DNS-Linear")    
            update_result_dict(Train_time, trainingTime, "DNS-Linear")
            update_result_dict(Distance_time, distanceTime, "DNS-Linear")
            update_result_dict(Evaluation_time, evalTime, "DNS-Linear") 
                
            
            
            
            ### RBF kernel
            
            null_rbf_kernel = NullSpace(True)
            
            startT = time.time()
            K_trn, mu = RBF_kernel(dataTrn)
            null_rbf_kernel.fit(K_trn,Y_trn)
            trainingTime = time.time() - startT
            print("Learned RBF Kernalized Null Space in {:.4f} seconds".format(trainingTime))
            if null_rbf_kernel.SVD_completed: 
                startT = time.time()
                K_probe, _ = RBF_kernel(dataTrn, dataProbe, mu)
                K_gallery, _ = RBF_kernel(dataTrn, dataGallery, mu)        
                dis_null_kernel_test = null_rbf_kernel.get_distance(K_probe, K_gallery)
                distanceTime = time.time() - startT
                
                startT = time.time()     
                null_kernel_cmc, null_kernel_rank  = cmc(dis_null_kernel_test, Y_probe , Y_gallery)
                null_kernel_mAP, null_kernel_AP,null_kernel_id_sorted_indecies  = mAPk(dis_null_kernel_test, Y_probe , Y_gallery, return_list = mAP_LIST)
                evalTime = time.time()-startT
                
                null_kernel_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, null_kernel_id_sorted_indecies, gallery_filenames, k)
            else:
                error_lst.append("DNS-RBF did not converge: Train {} - Fold {}".format(train_amount, k))
                distanceTime = -1.0
                evalTime = -1.0
                null_kernel_cmc = np.asarray([np.nan]*dataGallery.shape[0])
                null_kernel_mAP = np.asarray([np.nan]*dataGallery.shape[0])
                null_kernel_ids = {}

            update_result_dict(CMC_results, null_kernel_cmc, "DNS-RBF")
            update_result_dict(mAP_results, null_kernel_mAP, "DNS-RBF")
            update_ID_dict(incorrect_frames, null_kernel_ids, "DNS-RBF")    
            update_result_dict(Train_time, trainingTime, "DNS-RBF")
            update_result_dict(Distance_time, distanceTime, "DNS-RBF")
            update_result_dict(Evaluation_time, evalTime, "DNS-RBF") 
                
            
            
            
            
            
            
            ## P-norms
            
            startT = time.time()
            dis_l1_test = pairwise_distances(dataProbe, dataGallery, metric = "minkowski", p=1)
            distanceTime = time.time() - startT
            print("Calculated L1 distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            l1_cmc, l1_rank  = cmc(dis_l1_test, Y_probe, Y_gallery)
            l1_mAP, l1_AP, l1_id_sorted_indecies  = mAPk(dis_l1_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
                    
            l1_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, l1_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, l1_cmc, "L1")
            update_result_dict(mAP_results, l1_mAP, "L1")
            update_ID_dict(incorrect_frames, l1_ids, "L1")   
            update_result_dict(Distance_time, distanceTime, "L1")
            update_result_dict(Evaluation_time, evalTime, "L1") 
                
            
        
            startT = time.time()
            dis_l1_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "minkowski", p=1)
            distanceTime = time.time() - startT
            print("Calculated L1 distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            l1_cmc, l1_rank  = cmc(dis_l1_test, Y_probe, Y_gallery)
            l1_mAP, l1_AP, l1_id_sorted_indecies  = mAPk(dis_l1_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
                    
            l1_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, l1_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, l1_cmc, "L1-PCA")
            update_result_dict(mAP_results, l1_mAP, "L1-PCA")
            update_ID_dict(incorrect_frames, l1_ids, "L1-PCA")   
            update_result_dict(Distance_time, distanceTime, "L1-PCA")
            update_result_dict(Evaluation_time, evalTime, "L1-PCA") 
            
            
            
            
            startT = time.time()
            dis_l2_test = pairwise_distances(dataProbe, dataGallery, metric = "minkowski", p=2)
            distanceTime = time.time() - startT
            print("Calculated L2 distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            l2_cmc, l2_rank  = cmc(dis_l2_test, Y_probe, Y_gallery)
            l2_mAP, l2_AP, l2_id_sorted_indecies = mAPk(dis_l2_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            l2_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, l2_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, l2_cmc, "L2")
            update_result_dict(mAP_results, l2_mAP, "L2")
            update_ID_dict(incorrect_frames, l2_ids, "L2")   
            update_result_dict(Distance_time, distanceTime, "L2")
            update_result_dict(Evaluation_time, evalTime, "L2") 
                        
            
            
            startT = time.time()
            dis_l2_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "minkowski", p=2)
            distanceTime = time.time() - startT
            print("Calculated L2 distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            l2_cmc, l2_rank  = cmc(dis_l2_test, Y_probe, Y_gallery)
            l2_mAP, l2_AP, l2_id_sorted_indecies = mAPk(dis_l2_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time()-startT
            
            l2_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, l2_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, l2_cmc, "L2-PCA")
            update_result_dict(mAP_results, l2_mAP, "L2-PCA")
            update_ID_dict(incorrect_frames, l2_ids, "L2-PCA")   
            update_result_dict(Distance_time, distanceTime, "L2-PCA")
            update_result_dict(Evaluation_time, evalTime, "L2-PCA") 
                
            
            
            
            startT = time.time()
            dis_cd_test = pairwise_distances(dataProbe, dataGallery, metric = "cosine")
            distanceTime = time.time() - startT
            print("Calculated Cosine distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            cd_cmc, cd_rank  = cmc(dis_cd_test, Y_probe, Y_gallery)
            cd_mAP, cd_AP, cd_id_sorted_indecies  = mAPk(dis_cd_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time() - startT
            
            cd_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, cd_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, cd_cmc, "Cos. Dist.")
            update_result_dict(mAP_results, cd_mAP, "Cos. Dist.")
            update_ID_dict(incorrect_frames, cd_ids, "Cos. Dist.")  
            update_result_dict(Distance_time, distanceTime, "Cos. Dist.")
            update_result_dict(Evaluation_time, evalTime, "Cos. Dist.") 
                
        
            startT = time.time()
            dis_cd_test = pairwise_distances(dataPCAProbe, dataPCAGallery, metric = "cosine")
            distanceTime = time.time() - startT
            print("Calculated Cosine distance in {:.4f} seconds".format(distanceTime))
            
            startT = time.time()
            cd_cmc, cd_rank  = cmc(dis_cd_test, Y_probe, Y_gallery)
            cd_mAP, cd_AP, cd_id_sorted_indecies  = mAPk(dis_cd_test, Y_probe, Y_gallery, return_list = mAP_LIST)
            evalTime = time.time() - startT
            
            cd_ids = find_incorrect_ID_frames(Y_probe, Y_gallery, cd_id_sorted_indecies, gallery_filenames, k)
            
            update_result_dict(CMC_results, cd_cmc, "Cos. Dist.-PCA")
            update_result_dict(mAP_results, cd_mAP, "Cos. Dist.-PCA")
            update_ID_dict(incorrect_frames, cd_ids, "Cos. Dist.-PCA")  
            update_result_dict(Distance_time, distanceTime, "Cos. Dist.-PCA")
            update_result_dict(Evaluation_time, evalTime, "Cos. Dist.-PCA") 
                
        
        
        
        # Plot CMC and save results
        cmc_mean = np.zeros((16, dataGallery.shape[0]))
        cmc_std = np.zeros((16, dataGallery.shape[0]))
        mAP_mean = np.zeros((16, dataGallery.shape[0]))
        mAP_std = np.zeros((16, dataGallery.shape[0]))
        
        cmc_values = np.asarray(list(CMC_results.values()))
        mAP_values = np.asarray(list(mAP_results.values()))
        
        cmc_labels = list(CMC_results.keys())
        mAP_labels = list(CMC_results.keys())
        
        
        
        print("\nMethod | \t Rank 1 | \t Rank 5 | \t Rank 10 | \t Rank 15 | \t Rank 20 | \t mAP")
        for i in range(len(cmc_labels)):

            cmc_no_nan = cmc_values[i][~np.isnan(cmc_values[i]).any(axis=1)]
            mAP_no_nan = mAP_values[i][~np.isnan(mAP_values[i]).any(axis=1)]

            if len(cmc_no_nan) > 0:
                cmc_mean[i] = np.mean(cmc_no_nan, axis=0)
                mAP_mean[i] = np.mean(mAP_no_nan, axis=0)
                mAP_std[i] =  np.std(mAP_no_nan, axis=0)

            cmc_vals = cmc_mean[i] * 100
            if mAP_LIST:
                mAP_vals = mAP_mean[i][-1]
                mAP_stds = mAP_std[i][-1]
            else:
                mAP_vals = mAP_mean[i]
                mAP_stds = mAP_std[i]
            print(cmc_labels[i] + " | \t {0:.2f} | \t {1:.2f} | \t {2:.2f} | \t {3:.2f} | \t {4:.2f} | \t {5:.2f}".format(cmc_vals[0],cmc_vals[4],cmc_vals[9],cmc_vals[14],cmc_vals[19], mAP_vals))
            across_trn_mean[cmc_labels[i]].append(mAP_vals)
            across_trn_std[cmc_labels[i]].append(mAP_stds)
        
        file_suffix =  str(N_unique_labels) + "-FISH-"
        file_suffix += "TRN"
        
        CMC_top_k = np.unique(Y_tst_ind).shape[0]-Y_probe.shape[0]
        CMC_PLOT = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_CMC_{}.pdf".format(train_amount))
        cmc_labels_plot = cmc_labels.copy()
        for idx in range(len(cmc_labels)):
            cmc_labels_plot[idx] += " ({:.2f}% $\pm$ {:.2f})".format(cmc_mean[idx][0]*100,cmc_std[idx][0]*100)
        plotCurve(CMC_top_k, cmc_mean, cmc_labels_plot, cmc_labels,"CMS (%)", "Cumulative Matching Characteristic Curve - " + file_suffix, CMC_PLOT)
        
        if mAP_LIST:
            mAP_PLOT = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_mAP_{}.pdf".format(train_amount))
            mAP_labels_plot = mAP_labels.copy()
            for idx in range(len(mAP_labels)):
                mAP_labels_plot[idx] += " ({:.2f}% $\pm$ {:.2f})".format(mAP_mean[idx][-1]*100,mAP_std[idx][-1]*100)
            plotCurve(CMC_top_k, mAP_mean, mAP_labels_plot, mAP_labels, "mAP (%)", "mean Average Precision Curve - " + file_suffix, mAP_PLOT)
            
        
        # Save the results in csv and pkl files
        CMC_CSV = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_CMC_{}.csv".format(train_amount))
        CMC_STD_CSV = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_CMC_STD_{}.csv".format(train_amount))
        mAP_CSV = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_mAP_{}.csv".format(train_amount))
        mAP_STD_CSV = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_mAP_STD_{}.csv".format(train_amount)) 

        with open(CMC_CSV, 'w', newline = "") as csvWriteFile:
            writer = csv.writer(csvWriteFile, delimiter=";")
            writer.writerow(cmc_labels)
            
            for i in range(cmc_mean[0].shape[0]):
                row = []
                
                for j in range(len(cmc_mean)):
                    row.append(cmc_mean[j][i])
                
                writer.writerow(row)
                
        with open(CMC_STD_CSV, 'w', newline = "") as csvWriteFile:
            writer = csv.writer(csvWriteFile, delimiter=";")
            writer.writerow(cmc_labels)
            
            for i in range(cmc_std[0].shape[0]):
                row = []
                
                for j in range(len(cmc_std)):
                    row.append(cmc_std[j][i])
                
                writer.writerow(row)
                
                
        with open(mAP_CSV, 'w', newline = "") as csvWriteFile:
            writer = csv.writer(csvWriteFile, delimiter=";")
            writer.writerow(mAP_labels)
            
            if type(mAP_mean[0]) is np.ndarray:
                for i in range(mAP_mean[0].shape[0]):
                    row = []
                    
                    for j in range(len(mAP_mean)):
                        row.append(mAP_mean[j][i])
                    
                    writer.writerow(row)
            else:
                row = []
                
                for j in range(len(mAP_mean)):
                    row.append(mAP_mean[j])
                
                writer.writerow(row)
                
        with open(mAP_STD_CSV, 'w', newline = "") as csvWriteFile:
            writer = csv.writer(csvWriteFile, delimiter=";")
            writer.writerow(mAP_labels)
            
            if type(mAP_std[0]) is np.ndarray:
                for i in range(mAP_std[0].shape[0]):
                    row = []
                    
                    for j in range(len(mAP_std)):
                        row.append(mAP_std[j][i])
                    
                    writer.writerow(row)
            else:
                row = []
                
                for j in range(len(mAP_std)):
                    row.append(mAP_std[j])
                
                writer.writerow(row)
    

    file_suffix =  str(N_unique_labels) + "-FISH-"
    
    file_suffix += "TRN"
    mAP_values = np.asarray(list(across_trn_mean.values()))
    final_MmAP_PLOT = os.path.join(OUTPUT_DIR, FEATURE_NAME + "_mAP_Final.pdf")
    mAP_labels_plot = mAP_labels.copy()
    plotCurve(train_range, mAP_values, mAP_labels_plot, mAP_labels, "mAP (%)", "mean Average Precision Curve - " + file_suffix, final_MmAP_PLOT, x_label = "#Training samples")


    with open(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_FULL_mAP.pkl"), "wb") as f:
        pickle.dump({"mean": across_trn_mean, "std.dev.":across_trn_std}, f, protocol = pickle.HIGHEST_PROTOCOL)

    with open(os.path.join(OUTPUT_DIR, FEATURE_NAME + "_ERRORS.txt"), "w") as text_file:
        text_file.write("\n".join(error_lst))

        
# TODO : Add option to give random seed number
if __name__ == "__main__":
    
    ap = argparse.ArgumentParser(
        description = "Applies the different metric learning approaches on the supplied features")
    ap.add_argument("-directoryPath", "--directoryPath", type=str, default = "",
                help="Path to the directory with all the pkl files")
    ap.add_argument("-trainFeaturesPath", "--trainFeaturesPath", type=str, default = "2F_2L_TRN6_M_both_LOMO.pkl",
                help="Path to the training features that need to be analyzed")
    ap.add_argument("-trainLabelsPath", "--trainLabelsPath", type=str, default = "2F_2L_TRN6_M_both_LABELS.pkl",
                help="Path to the labels of the training features that need to be analyzed")
    ap.add_argument("-trainFilesPath", "--trainFilesPath", type=str, default = "2F_2L_TRN6_M_both_FILENAMES.pkl",
                help="Path to the files of the training features that need to be analyzed")
    ap.add_argument("-testFeaturesPath", "--testFeaturesPath", type=str, default = "",
                help="Path to the testing features that need to be analyzed")
    ap.add_argument("-testLabelsPath", "--testLabelsPath", type=str, default = "",
                help="Path to the labels of the testing features that need to be analyzed")
    ap.add_argument("-testFilesPath", "--testFilesPath", type=str, default = "",
                help="Path to the filenames of the testing features that need to be analyzed")
    ap.add_argument("-outputPath", "--outputPath", type=str, default = "",
                help="Path to output folder. If provided the .csv file will be saved at the specified location. Else it will be saved where this script is placed")
    ap.add_argument("-rootOutputName", "--rootOutputName", type=str, default = "Test",
                help="Root name of the to output files")
    ap.add_argument("-kDataSplit", "--kDataSplit", type=int, default = 10,
                help="Number of random data splits tested")
    ap.add_argument("-trainTestSplit", "--trainTestSplit", type=float, default = 0.5,
                help="Ratio of training and testing dataset, when not having a specific testing dataset")
    ap.add_argument("-pcaRatio", "--pcaRatio", type=float, default = 0.5,
                help="How large a ratio of the explained variance should be used for PCA")
    ap.add_argument("-nTrain", "--nTrain", type=int, default = None,
                help="Amount of images used per identity for training set")
    ap.add_argument("-nTest", "--nTest", type=int, default = 2,
                help="Amount of images used per identity for testing set")
    
    print("HEY")
    
    args = vars(ap.parse_args())
    print(args)
    evaluate_features(args)
    