# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund
"""

import numpy as np
import matplotlib.pyplot as plt

def cmc(D, Yp, Yg, dissimilarity = True):
    """
    Calculates the Cumulative Matching Characteristic (CMC) scores for the given distance matrix and labels.
    Supports several gallery samples per probe
    
    Based on the MATLAB code used by Shengcai Liao for the XQDA code
    
    Input:
        D: A distance matrix where the D[i,j] index is the distance between the ith probe and jth gallery
        Yp: Labels for the probe
        Yg: Labels for the gallery
        dissimilarity: A boolean values stating whether it is a dissimilarity or similarity value
    
    Output:
        CMC_scores: The calculated CMC values for the different top-n ranks
        rank: The top rank of each probe
    """
    
    D = D.copy()
    
    if not dissimilarity:
        D = -D
        
    ## Generate a binary mask which highlight which elements in the 
    ## distance matrix are the distance values of the actual pairs
    bLabels = Yp.reshape(-1,1) == Yg.reshape(1,-1)
    
    ## Check if any probes have more than one gallery sample, with the same label
    if np.sum(np.sum(bLabels, axis = 1)>1, dtype=np.int):
        print("There are more than one true gallery example for one or more probes")
        
    ## Check that all probes have one gallery sample, with the same label
    elif np.sum(np.sum(bLabels, axis = 1) == 0, dtype=np.int):
        print("There are one or more probes with no gallery samples")
    
    ## Find the indecies that sort the distance values in ascending order
    sortedIdx = D.argsort(axis=1)
    
    ## Find the samllest true distance value
    D[bLabels == False] = np.inf
    min_scores = D.min(axis=1)
    
    probe_count = Yp.shape[0]
    gallery_count = Yg.shape[0]

    ## Get the rank of all true distance values    
    rank = np.zeros(probe_count)
    for i in range(probe_count):
        rank[i] = np.where(np.isin(D[i,sortedIdx[i,:]], min_scores[i]))[0][0]
    
    ## Calculate the CMC values at different top-n rank values
    CMC_scores = np.zeros(gallery_count)
    for i in range(gallery_count):
        CMC_scores[i] = np.sum(rank <= i)
    CMC_scores /= probe_count
    
    return CMC_scores, rank


def mAPk(D, Yp, Yg, dissimilarity = True, k = None, return_list = False):
    """
    Calculates the Average Precisions (AP) and mean Average Precision (mAP) for the first k elements of the ranked distance matrix and labels.
    
    AP is calculated based on the finite case from https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Average_precision
    AP@k is calculated by only averaging the precision over the amount of relevant items in the first k elements
    
    Input:
        D: A distance matrix where the D[i,j] index is the distance between the ith probe and jth gallery
        Yp: Labels for the probe
        Yg: Labels for the gallery
        dissimilarity: A boolean values stating whether it is a dissimilarity or similarity value
        k: The amount of elements to be considered when calculating the AP and mAP. At default set to None, which results in all gallery samples being evaluated
        return_list: Whether the function should return lists, containing the mAP and AP of at each step up untill k. By default set to False
    
    Output:
        mAP: The mAP value when examining up untill the kth element. Is a list if return_list is True
        AP: The AP values for each probe up untill the kth element. Is a list of lists if return_list is True
        sortedIdx: The indeceis of the gallery images, sorted by their distance in ascending order
    """
    
    D = D.copy()
    
    if not dissimilarity:
        D = -D
    
    ## Determine the boolean values for each probe and gallery label combination
    bLabels = Yp.reshape(-1,1) == Yg.reshape(1,-1)
    
    ## Sort the distance matrix in ascending order, per row
    sortedIdx = D.argsort(axis=1)
    
    ## Re-order the binary label matrix, so that they are now ranked 
    ranked_list= np.zeros((bLabels.shape[0],bLabels.shape[1]), dtype=np.bool)
    for i in range(ranked_list.shape[0]):
        ranked_list[i,:] = bLabels[i,sortedIdx[i,:]]

    
    ## AP = (Sum of (precision@k * rel@k))/total_number_of_relevant_items
    ## where rel@k is 1 if the kth item is relevant, and zero otherwise
    ## and precision@k is calculated as:
    ## precision@k = number_of_relevant_items@k / k
    ## In theory any non retrived releveant items should have a precision of 0. In our case this wont matter
    n_probes = Yp.shape[0]
    
    if k == None or k < 1:
        k = Yg.shape[0]
    n_relevant = np.sum(ranked_list, axis=1)
    
    precision = np.zeros((n_probes,k))
    
    for j in range(n_probes):
        relevant = 0
        
        for i in range(k):
            
            ## Check if the ith gallery item of the jth probe have the same label
            ## if not just continue to the next gallery item
            if not ranked_list[j,i]:
                continue
            
            relevant += 1
            
            ## Precision is calculated as the amount of relevant items so far, 
            ## divided divided with the number of items retrieved
            precision[j,i] = relevant/(i+1)
        
            ## If all relevant items of the jth probe has been found, just stop, won't change the AP
            if relevant == n_relevant[j]:
                break
        
        
    ## Calculate AP@k and mAP@k as:
    ## AP@k = (Sum of (precision@k * rel@k))/relevant_items@k
    ## Where relevant_items@k is the amount of relevant_items encountered up untill
    ## and including the kth item
    
    if return_list:   
        AP = np.zeros((n_probes,k))
        mAP = np.zeros((k))
        for i in range(k):
            AP[:,i] = np.divide(np.sum(precision[:,:i+1],axis=1),np.sum(ranked_list[:,:i+1],axis=1))
            AP[np.isnan(AP)] = 0
            mAP[i] = np.mean(AP[:,i])
        return mAP, AP, sortedIdx
    
    else:
        AP = np.sum(precision,axis=1)/n_relevant
        AP[np.isnan(AP)] = 0
        mAP = np.mean(AP)        
        return mAP, AP, sortedIdx
    
    
def mAP(D, Yp, Yg, dissimilarity = True):  
    """
    Calculates the Average Precisions (AP) and mean Average Precision (mAP) for the ranked distance matrix and labels.
    
    AP is calculated based on the finite case from https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Average_precision
    
    Input:
        D: A distance matrix where the D[i,j] index is the distance between the ith probe and jth gallery
        Yp: Labels for the probe
        Yg: Labels for the gallery
        dissimilarity: A boolean values stating whether it is a dissimilarity or similarity value
    
    Output:
        mAP: The mAP value of the dataset
        AP: The AP values for each probe up.
        sortedIdx: The indeceis of the gallery images, sorted by their distance in ascending order
    """  
    return mAPk(D, Yp, Yg, dissimilarity)
        

def plotCurve(x_axis, y_axis, labels, method_labels, y_label = "", title="", output="", x_label = "Rank"):
    """
    Plots the given values
    
    Input:
        x_axis: Either a scalr value stating the number of x steps, or a list of x values
        y_axis: A list of lists or just a single lists of the y_values to be plotted
        labels: A list of strings, denoting the method used, each associated with a cmc_value
        output: A string determing where the output pdf should be saved
        
    Output:
        Outputs a plot of the  given values of the given methods
    """
    
    plt.figure(1,(7.5,2.5)) #second argument is size of figure in integers
    plt.clf()
    
    # Set the colors used for the plotting, so that no color is repeated
    #plt.gca().set_prop_cycle(plt.cycler('color', plt.cm.tab20(np.linspace(0, 1, len(labels)))))
    plt.cm.tab20
    
    # Multiply Y-values, assumed to be in range 0 to 1, so they are in range 0 - 100
    y_axis = np.asarray(y_axis) * 100
    
    if type(x_axis) is int:
        x_axis = [i for i in range(1,x_axis+1)]
    
    if type(y_axis) is list or type(y_axis) is np.ndarray:    
        for i in range(len(y_axis)):
            if method_labels[i] in ["L1", "L2", "Cos. Dist.", "L1-PCA", "L2-PCA", "Cos. Dist.-PCA"]:
                line = "--"
            else:
                line = "-"
            plt.plot(x_axis, y_axis[i][:x_axis[-1]], label = labels[i], linestyle = line)
    else:
        plt.plot(x_axis, y_axis, labels)
        

    plt.grid(which="major", alpha=1.0)
    plt.grid(which="minor", alpha=0.5)
    plt.gca().set_yticks(np.arange(0,101,20))
    plt.gca().set_yticks(np.arange(0,101,10),minor=True)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.xlim(np.min(x_axis), np.max(x_axis))
    plt.ylim(0, 105)
    
    plt.legend(bbox_to_anchor=(0.,-0.2, 1., -.052), loc=9,
           ncol=2, mode="expand", borderaxespad=0.)
    plt.title(title)
    plt.savefig(output, bbox_inches="tight")