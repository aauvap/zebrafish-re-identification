#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund

Implementation of the Ensemble of Localized Features feature descriptor first proposed by Gray and Tao (2008)
"""

import time
import cv2
import numpy as np
from scipy import misc
import skimage.color as scol
from skimage import feature

class ELF:
    def __init__(self, use_Color = True, use_Texture = True, use_Gabor = True, use_Schmid = True, use_LBP = True):
        self.use_Color = use_Color
        self.use_Texture = use_Texture
        self.use_Gabor = use_Gabor
        self.use_Schmid = use_Schmid
        self.use_LBP = use_LBP

        if use_Texture:
            self.size_filt = 47 # Support of largest filter(must be odd
            self.dataType = cv2.CV_64F
            if use_Gabor:
                self.g_kernels, self.max_Gabor, self.min_Gabor = self.getGaborFilters(self.size_filt, self.dataType)
                self.num_filt_gabor = self.g_kernels.shape[2]
            if use_Schmid:
                self.s_kernels = self.getSchmidFilter(self.size_filt)
                self.num_filt_schmid = self.s_kernels.shape[2]
            if use_LBP:
                self.num_filt_lbp = 1
                self.points = 8
                self.radius = 1
                self.lbp_type = "default"


    def ColorSpaces(self, oriImgs):
        """
        Extracts the RGB, HS and YCbCr color spaces for each input image in oriImgs.
        
        Input:
            oriImgs: Input data matrix of size [height, width, chan, numImgs]
            
        Output:
            colorSpaces: Output data matrix of size [height, width, numColSpaces, numImgs]
        """
        
        height, width, chan, numImgs = oriImgs.shape
        
        colorSpaces = np.empty((height, width, 8, numImgs))

        # Convert the whole images to RGB, HSV and YCbCr color spaces
        for image in range(int(numImgs)):

            I = oriImgs[:,:,:,image]
            I = I.astype(np.float64)/255

            rgbImg = I

            ycbcrImg = scol.rgb2ycbcr(I)

            hsvImg = scol.rgb2hsv(I)
            hsImg = hsvImg[:,:,:2] # Only H and S channels
            
            colorSpace = rgbImg
            colorSpace = np.concatenate((colorSpace, ycbcrImg), axis=2)
            colorSpace = np.concatenate((colorSpace, hsImg), axis=2)
            colorSpaces[:,:,:, image] = colorSpace

        return colorSpaces


    def _MakeSchmidFilter(self, size_filt,sigma,tau):
        """
        Makes the Schmidt filter with specified size, sigma and tau.
        Based on implementation by the VGG oxford group: http://www.robots.ox.ac.uk/~vgg/research/texclass/code/makeSfilters.m
        
        Input:
            size_filt: size of the filter
            sigma: Variance of the Gaussian envelope
            tau: number of cycles of the harmonic function within the Gaussian envelope of the filter
            
        Output:
            f: Output Schmid filter of size [size_filt, size_filt]
        """
        
        halfSize=(size_filt-1)/2
        x_mesh = np.arange(-halfSize, halfSize+1, 1)
        y_mesh = np.arange(-halfSize, halfSize+1, 1)
        x,y = np.meshgrid(x_mesh, y_mesh)
        r = (x*x+y*y)**0.5
        f = np.cos(r*(np.pi*tau/sigma))*np.exp(-(r*r)/(2*sigma*sigma))
        f = f - np.mean(np.reshape(f, (-1,1), order="F"))  # Pre-processing: zero mean
        f /= np.sum (np.absolute(np.reshape(f, (-1,1), order="F"))) # Pre-processing: L_{1} normalise
            
        return f


    def getSchmidFilter(self, size_filt=47):
        filters = np.zeros((size_filt, size_filt, 13)) #Allocate memory for N Schmid filters

        # Create all filters, as in "Viewpoint Invariant Pedestrian Recognition with an Ensemble of Localized Features" paper
        filters[:,:,0] = self._MakeSchmidFilter(size_filt,2,1)
        filters[:,:,1] = self._MakeSchmidFilter(size_filt,4,1)
        filters[:,:,2] = self._MakeSchmidFilter(size_filt,4,2)
        filters[:,:,3] = self._MakeSchmidFilter(size_filt,6,1)
        filters[:,:,4] = self._MakeSchmidFilter(size_filt,6,2)
        filters[:,:,5] = self._MakeSchmidFilter(size_filt,6,3)
        filters[:,:,6] = self._MakeSchmidFilter(size_filt,8,1)
        filters[:,:,7] = self._MakeSchmidFilter(size_filt,8,2)
        filters[:,:,8] = self._MakeSchmidFilter(size_filt,8,3)
        filters[:,:,9] = self._MakeSchmidFilter(size_filt,10,1)
        filters[:,:,10] = self._MakeSchmidFilter(size_filt,10,2)
        filters[:,:,11] = self._MakeSchmidFilter(size_filt,10,3)
        filters[:,:,12] = self._MakeSchmidFilter(size_filt,10,4)

        return filters


    def getGaborFilters(self, size_filt, dataType):
        # ksize - size of gabor filter (n, n) --> line width of road markings in pixel
        # sigma - standard deviation of the gaussian function
        # theta - orientation of the normal to the parallel stripes
        # lambda - wavelength of the sunusoidal factor
        # gamma - spatial aspect ratio
        # phi - phase offset
        # ktype - type and range of values that each pixel in the gabor kernel can hold
        
        filters = np.zeros((size_filt, size_filt, 8)) #Allocate memory for N Schmid filters

        filters[:,:,0] = cv2.getGaborKernel((size_filt, size_filt), np.sqrt(2), 0., 4., 0.3, 0., ktype=dataType)
        filters[:,:,1] = cv2.getGaborKernel((size_filt, size_filt), np.sqrt(2), 0., 8., 0.3, 0., ktype=dataType)
        filters[:,:,2] = cv2.getGaborKernel((size_filt, size_filt), 1., 0., 4., 0.4, 0., ktype=dataType)
        filters[:,:,3] = cv2.getGaborKernel((size_filt, size_filt), 1., 0., 8., 0.4, 0., ktype=dataType)
        filters[:,:,4] = cv2.getGaborKernel((size_filt, size_filt), np.sqrt(2), np.pi/2, 4., 0.3, 0., ktype=dataType)
        filters[:,:,5] = cv2.getGaborKernel((size_filt, size_filt), np.sqrt(2), np.pi/2, 8., 0.3, 0., ktype=dataType)
        filters[:,:,6] = cv2.getGaborKernel((size_filt, size_filt), 1., np.pi/2, 4., 0.4, 0., ktype=dataType)
        filters[:,:,7] = cv2.getGaborKernel((size_filt, size_filt), 1., np.pi/2, 8., 0.4, 0., ktype=dataType)

        max_Gabor = np.zeros((8))
        min_Gabor = np.zeros((8))

        for filt in range(8):
            kern = filters[:,:,filt]
            max_Gabor[filt] = kern[kern>=0].sum()
            min_Gabor[filt] = kern[kern<0].sum()

        return filters, max_Gabor, min_Gabor


    def FeatureDescriptors(self, images):
        """
        Calcualtes all the filtered image responses
        
        Input:
            images: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            gabor: Boolean whether the Gabor filters should be used
            schmid: Boolean whether the Schmid filters should be used
            makeSchmidFilter: Boolean whether the Schmid filters should be computed
            lbp: Boolean whether the Local Binary Pattern should be used
            
        Output:
            (descriptors_Gabor, min_Gabor, max_Gabor): The Gabor fitlered images, along with the min and max responses of the Gabor fitlers
            descriptors_Schmid: The Schmid fitlered images
            descriptors_LBP: The Local Binary Pattern response of the images
        """
        
        height, width, chan, numImgs = images.shape
        I = images
        I = I.astype(np.float64)/255.
        
        ## For ALL images
        descriptors_Gabor = np.zeros((height, width, self.num_filt_gabor, numImgs))
        descriptors_Schmid = np.zeros((height, width, self.num_filt_schmid, numImgs))
        descriptors_LBP = np.zeros((height, width, self.num_filt_lbp, numImgs))
        
        
        for image in range(int(numImgs)):
            
            # Get only the luminance channel Y of an image
            Y_channel = (scol.rgb2ycbcr(I[:,:,:,image]))[:,:,0]
            img_for_descriptor = Y_channel/235.

            ## Gabor Filter
            if self.use_Gabor:
                filtered_img_gabor = np.zeros((height, width, self.num_filt_gabor))

                for filt in range(self.num_filt_gabor):
                    filtered_img_gabor[:,:,filt] = cv2.filter2D(img_for_descriptor, self.dataType, self.g_kernels[:,:,filt])

                descriptors_Gabor[:,:,:,image] = filtered_img_gabor  
                
            ## Schmid Filter
            if self.use_Schmid:
                filtered_img_schmid = np.zeros((height, width, self.num_filt_schmid)) # Per image
            
                for filt in range(self.num_filt_schmid):
                    filtered_img_schmid[:,:,filt] = cv2.filter2D(img_for_descriptor, self.dataType, self.s_kernels[:,:,filt])

                descriptors_Schmid[:,:,:,image] = filtered_img_schmid
            
            ## Local Binary Pattern
            if self.use_LBP:                            
                lbp_img = feature.local_binary_pattern(img_for_descriptor, self.points, self.radius, self.lbp_type)
                filtered_img_lbp = lbp_img
                filtered_img_lbp = np.reshape(filtered_img_lbp, (height, width, self.num_filt_lbp))
                descriptors_LBP[:,:,:,image] = filtered_img_lbp

        return descriptors_Gabor, descriptors_Schmid, descriptors_LBP


    def CalculateHistograms(self, colorSpacesImgs=None, descriptors_Gabor=None, descriptors_Schmid=None, descriptors_LBP=None, stripe_num = 6, totalBins = 16):
        """
        Calcualtes the histograms of all the input response images
        
        Input:
            colorSpacesImgs: Data matrix consisting of the color spaces of size [height, width, chan, numImgs]
            descriptors_Gabor: Data matrix consisting of the Gabor responses of size [height, width, chan, numImgs]
            descriptors_Schmid: Data matrix consisting of the Schmid responses of size [height, width, chan, numImgs]
            descriptors_LBP: Data matrix consisting of the LBP responses of size [height, width, chan, numImgs]
            
        Output:
            features_all: A data matrix consisting of the concatenated histograms extracted of size [numImgs, numFeat] where numFeat is dependent of the input responses
        """
        
        
        if colorSpacesImgs is not None:
            height, width, chan, numImgs = colorSpacesImgs.shape
        elif descriptors_Gabor is not None:
            height, width, _, numImgs = descriptors_Gabor.shape
        elif descriptors_Schmid is not None:
            height, width, _, numImgs = descriptors_Schmid.shape
        elif descriptors_LBP is not None:
            height, width, _, numImgs = descriptors_LBP.shape
        
        stripe_height = np.int(height/stripe_num)

        features_color_all = None
        features_texture_all = None
        
        for image in range(numImgs):
            
            features_color_per_image = []
            features_texture_per_image = []
            
            for stripe in range(0, height-stripe_height+1, stripe_height):

                if colorSpacesImgs is not None:
                    
                    # COLOR HISTOGRAMS
                    for chan in range(colorSpacesImgs.shape[2]):
                        
                        # RGB
                        if chan < 3:
                            color_temp = colorSpacesImgs[stripe:stripe+stripe_height, :, chan, image]
                            feat, bins = np.histogram(color_temp, bins=totalBins, range=(0,1))
                            feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                            features_color_per_image.extend(feat)
        
                        #Y
                        if chan == 3:
                            color_temp = colorSpacesImgs[stripe:stripe+stripe_height, :, chan, image]
                            feat, bins = np.histogram(color_temp, bins=totalBins, range=(16,235))
                            feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                            features_color_per_image.extend(feat)
                        
                        #CBCR
                        if chan >= 4 and chan < 6:
                            color_temp = colorSpacesImgs[stripe:stripe+stripe_height, :, chan, image]
                            feat, bins = np.histogram(color_temp, bins=totalBins, range=(16,240))
                            feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                            features_color_per_image.extend(feat)
                        
                        #HS
                        if chan >=6:
                            color_temp = colorSpacesImgs[stripe:stripe+stripe_height, :, chan, image]
                            feat, bins = np.histogram(color_temp, bins=totalBins, range=(0,1))
                            feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                            features_color_per_image.extend(feat)
                
                #TEXTURE HISTOGRAMS                
                ## Schmid
                if descriptors_Schmid is not None:
                    for desc in range(descriptors_Schmid.shape[2]):
                        schmid_temp = descriptors_Schmid[stripe:stripe+stripe_height, :, desc, image]
                        feat, bins = np.histogram(schmid_temp, bins=totalBins, range=(-0.5,0.5))
                        feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                        features_texture_per_image.extend(feat)
                    
                ## Gabor    
                if descriptors_Gabor is not None:
                    for desc in range(descriptors_Gabor.shape[2]):                    
                        gabor_temp = descriptors_Gabor[stripe:stripe+stripe_height, :, desc, image]
                        feat, bins = np.histogram(gabor_temp, bins=totalBins, range=(self.min_Gabor[desc], self.max_Gabor[desc]))
                        feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                        features_texture_per_image.extend(feat)
                    
                ## LBP    
                if descriptors_LBP is not None:
                    for desc in range(descriptors_LBP.shape[2]):                    
                        lbp_temp = descriptors_LBP[stripe:stripe+stripe_height, :, desc, image]
                        feat, bins = np.histogram(lbp_temp, bins=totalBins, range=(0,255))
                        feat = feat/np.linalg.norm(feat, ord=1, keepdims=True)
                        features_texture_per_image.extend(feat)

                    
            ## Concatenate color histograms        
            if image == 0 and features_color_per_image != []:
                features_color_all = np.asarray(features_color_per_image)
                
            elif image > 0 and features_color_per_image != []:
                features_color_all = np.concatenate((features_color_all, np.asarray(features_color_per_image)), axis=0)
        
        
            ## Concatenate texture histograms
            if image == 0 and features_texture_per_image != []:
                features_texture_all = np.asarray(features_texture_per_image)
                
            elif image > 0 and features_texture_per_image != []:
                features_texture_all = np.concatenate((features_texture_all, np.asarray(features_texture_per_image)), axis=0)
                
        if features_color_all is not None:
            features_color_all = np.reshape(features_color_all, (numImgs,-1))  
            
        if features_texture_all is not None:
            features_texture_all = np.reshape(features_texture_all, (numImgs, -1))
            
        # Return color OR texture OR both features    
        if features_texture_all is None:
            return features_color_all
        elif features_color_all is None:
            return features_texture_all
        else:
            # I.e. We take all stacked color histograms from the patches, and then stacks with the texture histograms
            # meaning the histograms arent in order by patch. but shouldnt matter
            features_all = np.concatenate((features_color_all, features_texture_all), axis=1)
            return features_all
            
        
    def ELF(self, images, verbose = False):
        """
        Implementation of the Ensemble of Localized Features descriptor
        
        Input:
            images: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            use_Color:  Boolean on whether to extract color features
            use_Texture: Boolean on whether to extract texture features
            
        Output:
            histogramsImgs: A data matrix consisting of the extracted images size [numImgs, numFeat] where numFeat is dependent of the feature types used
        """
        
        t0 = time.time()
        colorSpacesImgs = None
        descriptors_Gabor = None
        descriptors_Schmid = None
        descriptors_LBP = None
        
        if self.use_Color:
            colorSpacesImgs = self.ColorSpaces(images)
        if self.use_Texture:
            descriptors_Gabor, descriptors_Schmid, descriptors_LBP = self.FeatureDescriptors(images)
            
        histogramsImgs = self.CalculateHistograms(colorSpacesImgs, descriptors_Gabor, descriptors_Schmid, descriptors_LBP)

        feaTime = time.time() - t0
        meanTime = feaTime / images.shape[3]

        if verbose:
            print('ELF feature extraction finished. Running time: {:.3f} seconds in total, {:.3f} seconds per image. Input image shape: {}'.format(feaTime, meanTime, images.shape))
        
        return histogramsImgs
    
            
            
    
if __name__ == "__main__":
    import os
    import glob

    path = os.getcwd()
    path_img = os.path.join(path, "images")
    
    images = []

    # Calculate the time needed for feature extractors to process all images
    t0 = time.time()

    # Read images
    for filename in sorted(glob.glob(os.path.join(path_img, "*.bmp"))):
        img = misc.imread(filename, mode="RGB")
        images.append(img)
    
    images = np.array(images)
    images = np.transpose(images, (1,2,3,0))

    ELF_CT = ELF()

    h1 = ELF_CT.ELF(images)
    h2 = ELF_CT.ELF(images[:,:,:,0].reshape((128,48,3,1)))
    h3 = ELF_CT.ELF(images[:,:,:,1].reshape((128,48,3,1)))
    print(np.allclose(h1[0],h2))
    print(np.allclose(h1[1],h3))
    
    ELF_T = ELF(use_Color=False)
    h4 = ELF_T.ELF(images)

    ELF_C = ELF(use_Texture=False)
    h5 = ELF_C.ELF(images)

    print(h1)

    img, dim = h1.shape
    with open("ELF.txt", "w") as f:
        for idxDim in range(dim):
            for idxImg in range(img):
                f.write("{:.15f}".format(h1[idxImg, idxDim])+"\t")
            f.write("\n")
    np.save("ELF", h1)