#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund

Reimplementation of the Local Maximal Occurrence feature descriptor by Liao et al. (2015)
The Retinex transformation used is not included
http://www.cbsr.ia.ac.cn/users/scliao/projects/lomo_xqda/
"""

import time
import cv2
import numpy as np
from scipy import misc
import skimage.color as scol

class LOMO:

    def __init__(self, use_Color = True, use_Texture = True, numScales = np.float64(3), blockSize = np.float64(10), blockStep = np.float64(5), hsvBins = np.float64([8, 8, 8]), tau = np.float64(0.3), R = np.array([3,5], dtype=np.int32), numPoints = np.float64(4)):
        self.use_Color = use_Color
        self.use_Texture = use_Texture

        self.numScales = numScales
        self.blockSize = blockSize
        self.blockStep = blockStep

        # Joint-HSV settings
        self.hsvBins = hsvBins

        # SILTP settings
        self.tau = tau
        self.R = R
        self.numPoints = numPoints

    def LOMO(self, images, verbose = False):
        """
        Local Maximal Occurrence feature descriptor
        Based on MATLAB implementation found at: http://www.cbsr.ia.ac.cn/users/scliao/projects/lomo_xqda/
        
        Input:
            images: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            
        Output:
            descriptors: A data matrix consisting of the extracted images size [numImgs, numFeat] where numFeat is dependent of the input image size and parameters set
        """
        
        # Calculate the time needed for feature extractors to process all images
        t0 = time.time()

        if self.use_Color:
            # Extract Joint HSV based LOMO descriptors
            fea1 = self.PyramidMaxJointHist(images, self.numScales, self.blockSize, self.blockStep, self.hsvBins)
            fea1 = np.array(fea1)

        if self.use_Texture:
            # Extract SILTP based LOMO descriptors
            fea2 = []

            for i in range(self.R.shape[0]):
                fea2.append(self.PyramidMaxSILTPHist(images, self.numScales, self.blockSize, self.blockStep, self.tau, self.R[i], self.numPoints))  # ok<AGROW>

            fea2 = np.array(fea2)
            fea2 = np.reshape(fea2, (-1,images.shape[3]), order="F")

        if self.use_Color and self.use_Texture:  
            # Concatenate two feature descriptors together
            descriptors = np.concatenate((fea1, fea2),axis=0)
        elif self.use_Color:
            descriptors = fea1
        elif self.use_Texture:
            descriptors = fea2
        else:
            raise ValueError("Neither color or texture features were calculated!")
        
        feaTime = time.time() - t0
        meanTime = feaTime / images.shape[3]

        if verbose:
            print('LOMO feature extraction finished. Running time: {:.3f} seconds in total, {:.3f} seconds per image. Input image shape: {}'.format(feaTime, meanTime, images.shape))
        
        return(descriptors.T)


    def PyramidMaxJointHist(self, oriImgs, numScales=3, blockSize=10, blockStep=5, colorBins=[8,8,8]):
        """
        Calcualtes the joint-HSV representation of the input images , calculated over several pyramid scales
        
        Input:
            oriImgs: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            numScales: Number of scales in the Pyramid
            blockSize: Width/height of the window
            blockStep: Number of pixels the window is moved per step
            colorBins: List with each element corresponding to number of bins for the channel.
            
        Output:
            descriptors: ???
        """
        ##  PyramidMaxJointHist: HSV-based LOMO representation

        totalBins = np.prod(colorBins)
        numImgs = oriImgs.shape[3]
        images = np.zeros(oriImgs.shape)
        
        colorBins = np.array(colorBins, dtype = np.float32)

        # Color transformation
        for i in range(int(numImgs)):
            I = oriImgs[:,:,:,i]
            I = scol.rgb2hsv(I)
            I = I.astype(np.float32)
            
            # Distribute HSV values to the bins. Output: bin index from 0 to 7
            I[:,:, 0] = np.minimum(np.floor(I[:,:,0] * colorBins[0]), colorBins[0]-1) 
            I[:,:, 1] = np.minimum(np.floor(I[:,:,1] * colorBins[1]), colorBins[1]-1)
            I[:,:, 2] = np.minimum(np.floor(I[:,:,2] * colorBins[2]), colorBins[2]-1)
            images[:,:,:, i] = I # HSV       
            
        minRow = 0
        minCol = 0
        descriptors = None
        
        # Scan multi - scale blocks and compute histograms
        for scale in range(int(numScales)):
            patterns = (images[:,:,2,:] * colorBins[1] * colorBins[0] + images[:,:,1,:] * colorBins[0] + (images[:,:,0,:])) # make different ranges for H, S,V values
            patterns = patterns.reshape(-1, numImgs, order="F")
            
            height = np.float32(images.shape[0])
            width = np.float32(images.shape[1])
            maxRow = height - blockSize
            maxCol = width - blockSize
            
            # This does not ignore columns if width or height is divided by 5
            col_mesh = np.arange(int(minCol), int(maxCol), int(blockStep))
            row_mesh = np.arange(int(minRow), int(maxRow), int(blockStep))
            cols, rows = np.meshgrid(col_mesh, row_mesh) # Top - left positions
            cols = np.reshape(cols,(-1,1), order="F") 
            rows = np.reshape(rows,(-1,1), order="F")
            
            numBlocks = np.float32(cols.shape[0])
            numBlocksCol = np.float64(np.ceil(maxCol/blockStep)) # Missing /minCol, because it is 0

            if numBlocks == 0:
                break

            tens = np.arange(blockSize).T
            offset = np.zeros((int(blockSize), int(blockSize)))
            for i in range(int(blockSize)):
                offset[:, i] = tens + i * height # Offset to the top-left positions. blockSize-by-blockSize
            
            index = np.ravel_multi_index((rows, cols), (int(height),int(width)), order="F")

            offset = np.reshape(offset.astype(int), (-1,1), order="F")
            offset = np.tile(offset, index.shape[0])
            index = np.tile(index, offset.shape[0])
            index = (index.T + offset) # (blockSize*blockSize)-by-numBlocks

            patches = patterns[index.reshape((-1,), order="F"),:] # (blockSize * blockSize * numBlocks) - by - numImgs

            patches = patches.reshape(-1, int(numBlocks * numImgs), order="F") # (blockSize * blockSize) - by - (numBlocks * numChannels * numImgs)

            feat = []
            for patch in patches.T:
                fea, bins = np.histogram(patch, bins=int(totalBins), range=(0, totalBins))
                feat.append(fea)
            feat = np.array(feat)
            feat = feat.T
            
            fea = np.reshape(feat, (int(totalBins), int(numBlocks / numBlocksCol), int(numBlocksCol), int(numImgs)), order="F")
            fea = fea.max(2)
            fea = fea.reshape(-1, numImgs, order="F")
            
            if scale == 0:
                descriptors = fea
            
            if scale > 0:
                descriptors = np.concatenate((descriptors, fea), axis=0) 

            if scale < numScales:
                images = self.ColorPooling(images, 'max')

        descriptors = np.log(descriptors+1)
        descriptors /= np.linalg.norm(descriptors, ord = 2, axis = 0, keepdims = True)
        
        return descriptors


    def ColorPooling(self, images, method):
        """
        Performs pooling on HSV input images
        
        Input:
            images: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            method: Either 'average' or 'max', to determine type of pooling used
            
        Output:
            outImages: Downscaled images of size [height/2, width/2, chan, numImgs]
        """
        
        (height, width, numChannels, numImgs) = images.shape
        outImages = images

        if height % 2 == 1:
            outImages = outImages[:-1,:,:,:]
            height = height - 1

        if width % 2 == 1:
            outImages = outImages[:,:-1,:,:]
            width = width - 1

        if height == 0 or width == 0:
            print('Over scaled image: height=%d, width=%d.' % (height, width))

        height = np.int64(height / 2)
        width = np.int64(width / 2)

        outImages = outImages.reshape((2, height, 2, width, numChannels, numImgs), order="F")
        outImages = np.transpose(outImages, (1, 3, 4, 5, 0, 2))
        outImages = outImages.reshape((height, width, numChannels, numImgs, 2 * 2), order="F")
        
        if method == 'average':
            outImages = outImages.astype(np.float32)
            outImages = np.floor(np.mean(outImages, axis=4))
            
        elif method == 'max':
            outImages = outImages.astype(np.float32)
            outImages = np.amax(outImages, axis=4)

        else:
            print('Error pooling method: %s.' % method)

        return outImages
        
    
    def PyramidMaxSILTPHist(self, oriImgs, numScales=3, blockSize=10, blockStep=5, tau=0.3, R=5, numPoints=4 ):
        """
        Calcualtes the SILTP representation of the input images, calculated over several pyramid scales
        
        Input:
            oriImgs: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            numScales: Number of scales in the Pyramid
            blockSize: Width/height of the window
            blockStep: Number of pixels the window is moved per step
            tau: A scale factor indicating the comparison range
            R: The radius of pixels considered
            numPoints: Number of points considered in the SILTP calculation
            
        Output:
            descriptors: ???
        """    
        # PyramidMaxSILTPHist: SILTP-based LOMO representation

        totalBins = 3 ** numPoints

        (imgHeight, imgWidth, chan, numImgs) = oriImgs.shape
        images = np.zeros((imgHeight, imgWidth, numImgs))

        # Convert gray images
        for i in  range(int(numImgs)):
            I = oriImgs[:,:,:, i]
            I = cv2.cvtColor(I, cv2.COLOR_RGB2GRAY)
            I = I.astype(np.float32)
            images[:,:,i] = I/255

        minRow = 0
        minCol = 0
        descriptors = None

        # Scan multi - scale blocks and compute histograms
        for scale in range(int(numScales)):
            height = np.float32(images.shape[0])
            width = np.float32(images.shape[1])

            if width < R * 2 + 1:
                print("Skip scale R = %d, width = %d" % (R, width))
                continue

            patterns = self.SILTP(images, tau, R, numPoints)
            patterns = patterns.reshape(-1, numImgs, order="F")
            
            maxRow = height - blockSize
            maxCol = width - blockSize
            
            col_mesh = np.arange(int(minCol), int(maxCol), int(blockStep))
            row_mesh = np.arange(int(minRow), int(maxRow), int(blockStep))
            cols, rows = np.meshgrid(col_mesh, row_mesh) # Top - left positions
            cols = np.reshape(cols,(-1,1), order="F") # This does not miss columns if width or height is divided by 5
            rows = np.reshape(rows,(-1,1), order="F")

            numBlocks = np.float32(cols.shape[0])
            numBlocksCol = np.float64(np.ceil(maxCol/blockStep)) # Missing /minCol, because it is 0

            if numBlocks == 0:
                break

            tens = np.arange(blockSize).T
            offset = np.zeros((int(blockSize), int(blockSize)))
            for i in range(int(blockSize)):
                offset[:, i] = tens + i * height  # Offset to the top-left positions. blockSize-by-blockSize

            index = np.ravel_multi_index((rows, cols), (int(height),int(width)), order="F")
            
            offset = np.reshape(offset.astype(int), (-1,1), order="F")
            offset = np.tile(offset, index.shape[0])
            index = np.tile(index, offset.shape[0])
            index = index.T + offset  # (blockSize*blockSize)-by-numBlocks

            patches = patterns[index.reshape((-1,), order="F"),:]  # (blockSize * blockSize * numBlocks) - by - numImgs

            patches = patches.reshape(-1, int(numBlocks * numImgs), order="F") # (blockSize * blockSize) - by - (numBlocks * numChannels * numImgs)
            
            feat =[]
            for patch in patches.T:
                fea, bins = np.histogram(patch, bins=int(totalBins), range=(0, totalBins))
                feat.append(fea)
            feat = np.array(feat)
            feat = feat.T # totalBins - by - (numBlocks * numImgs)
            
            fea = np.reshape(feat, (int(totalBins), int(numBlocks / numBlocksCol), int(numBlocksCol), int(numImgs)), order="F")
            fea = fea.max(2)
            fea = np.reshape(fea, (-1, numImgs), order="F")
            
            if scale == 0:
                descriptors = fea
            
            if scale > 0:
                descriptors = np.concatenate((descriptors, fea), axis=0) 

            if scale < numScales:
                images = self.Pooling(images, 'average')


        descriptors = np.log(descriptors + 1)
        descriptors /= np.linalg.norm(descriptors, ord = 2, axis = 0, keepdims = True)

        return descriptors


    def Pooling(self, images, method):
        """
        Performs pooling on input input images
        
        Input:
            images: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            method: Either 'average' or 'max', to determine type of pooling used
            
        Output:
            outImages: Downscaled images of size [height/2, width/2, chan, numImgs]
        """
        
        (height, width, numImgs) = images.shape
        outImages = images

        if height % 2 == 1:
            outImages = outImages[:-1,:,:]
            height = height - 1


        if width % 2 == 1:
            outImages = outImages[:,:-1,:]
            width = width - 1


        if height == 0 or width == 0:
            print('Over scaled image: height= %d, width= %d.' % (height, width))


        height = np.int64(height / 2)
        width = np.int64(width / 2)

        outImages = outImages.reshape((2, height, 2, width, numImgs),order='F')
        outImages = np.transpose(outImages, (1, 3, 4, 0, 2))
        outImages = outImages.reshape((height, width, numImgs, 2 * 2), order="F")

        if method == 'average':
            outImages = outImages.astype(np.float32)
            outImages = np.mean(outImages, axis=3)
        elif method == 'max':
            outImages = outImages.astype(np.float32)
            outImages = np.amax(outImages, axis=3)
        else:
            print('Error pooling method: %s.' % method)

        return outImages


    def SILTP(self, I, tau=0.03, R=1, numPoints=4, encoder=0):
        """
        Calcualtes the Scale Invariant Local Ternary Pattern
            Note: Scale refers to the pixel intensity, and not object scale
        
        Input:
            I: Data matrix consisting of the input images of size [height, width, chan, numImgs]
            tau: A scale factor indicating the comparison range
            R: The radius of pixels considered
            numPoints: Number of points considered in the SILTP calculation
            encoder: How to represent the ternary points
            
        Output:
            J: SILTP images of size [height/2, width/2, chan, numImgs]
        """
        # Check parameters
        if tau <= 0 or np.floor(R) != R or R < 1 or not(numPoints==4 or numPoints==8) or not(encoder == 0 or encoder == 1):
            print('Error parameter values!')


        # Check image(s) size
        (h, w, n) = I.shape
        if h < 2*R+1 or w < 2*R+1:
            print('Too small image or too large R!')


        # Put the image(s) in a larger container
        I0 = np.zeros((h+2*R, w+2*R, n))
        I0[R:I0.shape[0]-R, R:I0.shape[1]-R, :] = I

        # Replicate border image pixels to the outer area
        I0[0:R,:,:] = np.tile(I0[R,:,:], (R,1,1)) #first row
        I0[I0.shape[0]-R:,:,:] = np.tile(I0[I0.shape[0]-R-1,:,:], (R,1,1)) #last row
        I0[:,0:R,:] = (np.tile(I0[:,R,:], (R,1,1))).transpose(1,0,2) #first col 
        I0[:,I0.shape[1]-R:,:] = (np.tile(I0[:,I0.shape[1]-R-1,:], (R,1,1))).transpose(1,0,2) #last column


        # Copy image(s) in specified directions
        I1 = I0[R:-R, 2*R:, :] 
        I3 = I0[0:-(2*R), R:-R, :]
        I5 = I0[R:-R, 0:-(2*R), :]
        I7 = I0[(2*R):, R:-R, :]
        

        if numPoints == 8:
            I2 = I0[0:-(2*R), 2*R:, :]
            I4 = I0[0:-(2*R), 0:-(2*R), :]
            I6 = I0[2*R:, 0:-(2*R), :]
            I8 = I0[2*R:, 2*R:, :]


        # Compute the upper and lower range
        L = (1-tau) * I
        U = (1+tau) * I

        # Compute the scale invariant local ternary patterns
        if encoder == 0:
            if numPoints == 4:
                J = (I1 < L) + (I1 > U) * 2 + ((I3 < L) + (I3 > U) * 2) * 3 + ((I5 < L) + (I5 > U) * 2) * 3**2 + ((I7 < L) + (I7 > U) * 2) * 3**3
            else:
                J = (I1 < L) + (I1 > U) * 2 + ((I2 < L) + (I2 > U) * 2) * 3 + ((I3 < L) + (I3 > U) * 2) * 3**2 + ((I4 < L) + (I4 > U) * 2) * 3**3 \
                    + ((I5 < L) + (I5 > U) * 2) * 3**4 + ((I6 < L) + (I6 > U) * 2) * 3**5 + ((I7 < L) + (I7 > U) * 2) * 3**6 + ((I8 < L) + (I8 > U) * 2) * 3**7

        else:
            if numPoints == 4:
                J = (I1 > U) + (I1 < L) * 2 + (I3 > U) * 2**2 + (I3 < L) * 2**3 + (I5 > U) * 2**4 + (I5 < L) * 2**5 + (I7 > U) * 2**6 + (I7 < L) * 2**7
            else:
                J = (I1 > U) + (I1 < L) * 2 + (I2 > U) * 2**2 + (I2 < L) * 2**3 + (I3 > U) * 2**4 + (I3 < L) * 2**5 + (I4 > U) * 2**6 + (I4 < L) * 2**7 +\
                    (I5 > U) * 2**8 + (I5 < L) * 2**9 + (I6 > U) * 2**10 + (I6 < L) * 2**11 + (I7 > U) * 2**12 + (I7 < L) * 2**13 + (I8 > U) * 2**14 + (I8 < L) * 2**15
        
        return J


if __name__ == "__main__":
    import os
    import glob

    path = os.getcwd()
    print (path)
    path_img = os.path.join(path, "images")
    
    images = []
    # Read images
    for filename in sorted(glob.glob(os.path.join(path_img, "*.bmp"))):
        img = misc.imread(filename, mode="RGB") #num, rows, cols, channels (height * width)
        images.append(img)
    
    images = np.array(images)
    images = np.transpose(images, (1,2,3,0))
    
    # Extract features. Run with a set of images is usually faster than that one by one, but requires more memory.
    LOMO_CT = LOMO()

    descriptorsCT = LOMO_CT.LOMO(images)
    d1 = LOMO_CT.LOMO(images[:,:,:,0].reshape((128,48,3,1)))
    d2 = LOMO_CT.LOMO(images[:,:,:,1].reshape((128,48,3,1)))
    print(np.allclose(d1,descriptorsCT[0]))
    print(np.allclose(d2,descriptorsCT[1]))

    LOMO_C = LOMO(use_Color=True, use_Texture=False)
    LOMO_T = LOMO(use_Color=False, use_Texture=True)

    descriptorsC = LOMO_C.LOMO(images)
    descriptorsT = LOMO_T.LOMO(images)

    print(descriptorsC.shape)
    print(descriptorsCT.shape)
    print(descriptorsT.shape)
    
    print(np.allclose(descriptorsC,descriptorsCT[:,:20480]))
    print(np.allclose(descriptorsT,descriptorsCT[:,20480:]))
    
    img, dim = descriptorsCT.shape
    with open("LOMO.txt", "w") as f:
        for idxDim in range(dim):
            for idxImg in range(img):
                f.write("{:.15f}".format(descriptorsCT[idxImg, idxDim])+"\t")
            f.write("\n")
    np.save("LOMO", descriptorsCT)