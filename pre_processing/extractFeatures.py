# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund
"""

import os
import cv2
import sys
import pandas as pd
import numpy as np
import argparse
import pickle


sys.path.append('../')
from feature_descriptors.ELF import ELF
from feature_descriptors.LOMO import LOMO

def rotate(img, angle, center):
    """
    Rotates the input image by the given angle around the given center
    
    Input:
        img: Input image
        angle: Angle in degrees
        center: Tuple consisting of the center the image should be rotated around
        
    Output:
        dst: The rotated image
    """
    
    rows, cols = img.shape[:2]
    
    M = cv2.getRotationMatrix2D(center,angle,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    return dst
    

def analyse_dataframe_by_tag(dataframe, ORIENTATION, rotated = False):
    stat_lst = {}
    for fish in ["Zebrafish"]:
        stats = {}
        
        fd = dataframe

        if ORIENTATION == "right":
            fd = fd[fd["Right"] == 1]
        elif ORIENTATION == "left":
            fd = fd[fd["Right"] == 0]
        
        if rotated:
            bb_height =  fd["Rotation Lower right corner Y"] - fd["Rotation Upper left corner Y"]
            bb_width =  fd["Rotation Lower right corner X"] - fd["Rotation Upper left corner X"]
        
        else:
            bb_height =  fd["Lower right corner Y"] - fd["Upper left corner Y"]
            bb_width =  fd["Lower right corner X"] - fd["Upper left corner X"]
        
        stats["FishType"] = fish
        stats["BBWidth_Mean"] = bb_width.mean(axis=0)
        stats["BBWidth_Std"] = bb_width.std(axis=0)
        stats["BBWidth_Median"] = bb_width.median(axis=0)
        stats["BBHeight_Mean"] = bb_height.mean(axis=0)
        stats["BBHeight_Std"] = bb_height.std(axis=0)
        stats["BBHeight_Median"] = bb_height.median(axis=0)
        
        stats["Orientation_Right"] = fd["Right"].sum()
        stats["Orientation_Left"] = fd["Right"].size - fd["Right"].sum()
        stats["Ids"] = fd["Object ID"].nunique()
        
        stat_lst[fish] = stats
        
    return stat_lst


def analyse_dataframe_by_ID(dataframe, ORIENTATION, rotated = False):
    stat_lst = {}
    IDs = dataframe["Object ID"].unique()
    N_IDs = dataframe["Object ID"].nunique()
    print(IDs)
    
    for idx in range(N_IDs):
        stats = {}
        
        fd = dataframe[dataframe["Object ID"] == IDs[idx]]
        
        if ORIENTATION == "right":
            fd = fd[fd["Right"] == 1]
        elif ORIENTATION == "left":
            fd = fd[fd["Right"] == 0]
        
        if rotated:
            bb_height =  fd["Rotation Lower right corner Y"] - fd["Rotation Upper left corner Y"]
            bb_width =  fd["Rotation Lower right corner X"] - fd["Rotation Upper left corner X"]
        
        else:
            bb_height =  fd["Lower right corner Y"] - fd["Upper left corner Y"]
            bb_width =  fd["Lower right corner X"] - fd["Upper left corner X"]
        
        
        stats["FishType"] = "Zebrafish"
        stats["BBWidth_Mean"] = bb_width.mean(axis=0)
        stats["BBWidth_Std"] = bb_width.std(axis=0)
        stats["BBWidth_Median"] = bb_width.median(axis=0)
        stats["BBHeight_Mean"] = bb_height.mean(axis=0)
        stats["BBHeight_Std"] = bb_height.std(axis=0)
        stats["BBHeight_Median"] = bb_height.median(axis=0)

        
        stats["Orientation_Right"] = fd["Right"].sum()
        stats["Orientation_Left"] = fd["Right"].size - fd["Right"].sum()
        stats["ID"] = fd["Object ID"].nunique()
        
        stat_lst["Zebrafish" + str(IDs[idx])] = stats
        
    return stat_lst


def extract_features(args):
    """
    Extract the features for both the original and rotated annotations. 
    The annotations are reshaped into the median size of the Zebrafish
    
    Currently applies:
        LOMO feature descriptor
    
    Input:
        args: A dictionary, holding the fields ['imageFolder', 'annotationFile', 'rotatedAnnotationFile', 'borderPad', 'orientation', 'flip', 'datasetName', 'outputPath']
    
    Output:
        Saves a separate .pkl file with Labels and the matrix of extracted features, for each annotation file
    """
    
    IMAGE_FOLDER = args["imageFolder"]
    ANNOTATION_FILE = args["annotationFile"]
    OUTPUT_DIR = args["outputPath"]
    BORDER_PAD = args["borderPad"]
    ORIENTATION = args["orientation"].lower()
    FLIP_FISH = args["flip"]
    DATASET = args["datasetName"]
    RESIZED_WIDTH = args["resizedWidth"]
    RESIZED_HEIGHT = args["resizedHeight"]
    RESIZED_WIDTH_ROTATED = args["resizedWidthRotated"]
    RESIZED_HEIGHT_ROTATED = args["resizedHeightRotated"]
    INCLUDE_GLITCHES = args["includeGlitches"]
    INCLUDE_OCCLUSIONS = args["includeOcclusions"]
    INCLUDE_TURNING = args["includeTurning"]

    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
    
    # Load the annotation CSV files as pandas dataframes
    fish_dataframe = pd.read_csv(ANNOTATION_FILE, sep=";")

    if not INCLUDE_OCCLUSIONS:
        fish_dataframe = fish_dataframe[fish_dataframe["Occlusion"] == 0]
    if not INCLUDE_TURNING:
        fish_dataframe = fish_dataframe[fish_dataframe["Turning"] == 0]
    if not INCLUDE_GLITCHES:
        fish_dataframe = fish_dataframe[fish_dataframe["Glitch"] == 0]

    fish_dataframe = fish_dataframe.reset_index(drop=True)


    ## Calculate some statstics for the two annotation files
    stat_lst = analyse_dataframe_by_tag(fish_dataframe, ORIENTATION)
    stat_lst_ID = analyse_dataframe_by_ID(fish_dataframe, ORIENTATION)

    stat_rotated_lst = analyse_dataframe_by_tag(fish_dataframe, ORIENTATION, True)
    stat_rotated_lst_ID = analyse_dataframe_by_ID(fish_dataframe, ORIENTATION, True)
    
    flipped = ""
    if FLIP_FISH:
        flipped = "_Flipped"
                    
    with open(os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_STATS.txt"), "w") as text_file:
        for key in stat_lst.keys():
            text_file.write(key + ": " + str(stat_lst[key]) + "\n")
            
    with open(os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_STATS_ID.txt"), "w") as text_file:
        for key in stat_lst_ID.keys():
            text_file.write(key + ": " + str(stat_lst_ID[key]) + "\n")
    
    with open(os.path.join(OUTPUT_DIR,  DATASET + "_Rotated_" + ORIENTATION + flipped +"_STATS.txt"), "w") as text_file:
        for key in stat_rotated_lst.keys():
            text_file.write(key + ": " + str(stat_rotated_lst[key]) + "\n")       

    with open(os.path.join(OUTPUT_DIR,  DATASET + "_Rotated_" + ORIENTATION + flipped +"_STATS_ID.txt"), "w") as text_file:
        for key in stat_rotated_lst_ID.keys():
            text_file.write(key + ": " + str(stat_rotated_lst_ID[key]) + "\n")
    
            
    LOMO_Feat = True
    LOMOSILTP_Feat = True
    LOMOHSV_Feat = True    
    ELF_Feat = True
    ELFCOLOR_Feat = True
    ELFTEXTURE_Feat = True
    
    
		
    # Define the resize size based on the median size of the zebrafish
    if RESIZED_WIDTH is None:
        resize_width = int(stat_lst["Zebrafish"]["BBWidth_Median"])
    else:
        resize_width = RESIZED_WIDTH
        
    if RESIZED_HEIGHT is None:
        resize_height = int(stat_lst["Zebrafish"]["BBHeight_Median"])
    else:
        resize_height = RESIZED_HEIGHT
    
    if RESIZED_WIDTH_ROTATED is None:
        resize_width_rotated = int(stat_rotated_lst["Zebrafish"]["BBWidth_Median"])
    else:
        resize_width_rotated = RESIZED_WIDTH_ROTATED
        
    if RESIZED_HEIGHT_ROTATED is None:
        resize_height_rotated = int(stat_rotated_lst["Zebrafish"]["BBHeight_Median"])
    else:
        resize_height_rotated = RESIZED_HEIGHT_ROTATED

    max_tmps = 500

    # Setup variables for feature extraction
    LOMO_features = None
    LOMOSILTP_features = None
    LOMOHSV_features = None
    ELF_features = None
    ELFCOLOR_features = None
    ELFTEXTURE_features = None
    LOMO_features_rotated = None
    LOMOSILTP_features_rotated = None
    LOMOHSV_features_rotated = None
    ELF_features_rotated = None
    ELFCOLOR_features_rotated = None
    ELFTEXTURE_features_rotated = None

    ELFCT = ELF(use_Color = True, use_Texture = True)
    ELFC = ELF(use_Color = True, use_Texture = False)
    ELFT = ELF(use_Color = False, use_Texture = True)
    LOMOCT = LOMO(use_Color = True, use_Texture = True)
    LOMOC = LOMO(use_Color = True, use_Texture = False)
    LOMOT = LOMO(use_Color = False, use_Texture = True)

    labels = []
    filenames = []

    orientation = []
    occlusion = []
    glitch = []
    turning = []

    tmp_imgs = []
    tmp_rot_imgs = []

    valid_images = [".png", ".jpg"]

    for file in sorted(os.listdir(IMAGE_FOLDER)):
        ext = os.path.splitext(file)[1]
        if ext.lower() not in valid_images:
            continue
        
        # Load the annotations for the file
        annotations = fish_dataframe[fish_dataframe["Filename"] == file]
        if ORIENTATION == "right":
            annotations = annotations[annotations["Right"] == 1]
        elif ORIENTATION == "left":
            annotations = annotations[annotations["Right"] == 0]
            
        if annotations.shape[0] == 0:
            continue
            
        # Load the image and the related annotation properties
        img = cv2.imread(os.path.join(IMAGE_FOLDER, file),1)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        IDs = annotations["Object ID"].values
        
        ULX = annotations["Upper left corner X"].values - BORDER_PAD
        ULY = annotations["Upper left corner Y"].values - BORDER_PAD
        LRX = annotations["Lower right corner X"].values + BORDER_PAD
        LRY = annotations["Lower right corner Y"].values + BORDER_PAD
        ULX[ULX < 0] = 0
        ULY[ULY < 0] = 0
        LRX[LRX > img.shape[1]-1] = img.shape[1]-1
        LRY[LRY > img.shape[0]-1] = img.shape[0]-1

        
        R_ULX = annotations["Rotation Upper left corner X"].values - BORDER_PAD
        R_ULY = annotations["Rotation Upper left corner Y"].values - BORDER_PAD
        R_LRX = annotations["Rotation Lower right corner X"].values + BORDER_PAD
        R_LRY = annotations["Rotation Lower right corner Y"].values + BORDER_PAD
        R_ULX[R_ULX < 0] = 0
        R_ULY[R_ULY < 0] = 0
        R_LRX[R_LRX > img.shape[1]-1] = img.shape[1]-1
        R_LRY[R_LRY > img.shape[0]-1] = img.shape[0]-1

        theta = annotations["Angle"].values
        c_x = annotations["Rotation center X"].values
        c_y = annotations["Rotation center Y"].values
        
        ORI = annotations["Right"].values
        OCC = annotations["Occlusion"].values
        GLI = annotations["Glitch"].values
        TUR = annotations["Turning"].values
        
        # Go through each annotation for the image, and resize + flip if necessary
        for idx in range(len(IDs)):
            bbox = (int(ULX[idx]), int(ULY[idx]), int(LRX[idx]), int(LRY[idx]))
            img_crop = img[bbox[1]:bbox[3], bbox[0]:bbox[2]]
            img_crop_resized = cv2.resize(img_crop, (resize_width, resize_height))
            img_crop_resized = np.divide(img_crop_resized, 255) #Image is integers, but we transform to floats later for LOMO
                        
            # Rotated image
            rot_img = rotate(img, theta[idx], center = (c_x[idx], c_y[idx]))
            
            rot_bbox = (int(R_ULX[idx]), int(R_ULY[idx]), int(R_LRX[idx]), int(R_LRY[idx]))
            rot_img_crop = rot_img[rot_bbox[1]:rot_bbox[3], rot_bbox[0]:rot_bbox[2]]
            rot_img_crop_resized = cv2.resize(rot_img_crop, (resize_width_rotated, resize_height_rotated))
            rot_img_crop_resized = np.divide(rot_img_crop_resized, 255) #Image is integers, but we transform to floats later for LOMO

            if FLIP_FISH and ORI[idx]:
                img_crop_resized = np.fliplr(img_crop_resized)
                rot_img_crop_resized = np.fliplr(rot_img_crop_resized)
            
            # Store the resized bounding boxes in a list, for later feature extraction
            tmp_imgs.append(img_crop_resized)
            tmp_rot_imgs.append(rot_img_crop_resized)
            labels.append(IDs[idx]-1) #Minus one to have it start from 0
            filenames.append(file)
            orientation.append(ORI[idx])
            occlusion.append(OCC[idx])
            glitch.append(GLI[idx])
            turning.append(TUR[idx])
            
            
        # When there are more than X images in the img list, extract the features from them
        if len(tmp_imgs) > max_tmps and (LOMO_Feat or LOMOSILTP_Feat or LOMOHSV_Feat or ELF_Feat or ELFCOLOR_Feat or ELFTEXTURE_Feat):
            n_bboxes = len(tmp_imgs)

            tmp_imgs = np.asarray(tmp_imgs)
            tmp_imgs = tmp_imgs.astype(np.float32) # Convert to float32 because LOMO uses opencv color conversion
            tmp_imgs = tmp_imgs.transpose(1,2,3,0)

            tmp_rot_imgs = np.asarray(tmp_rot_imgs)
            tmp_rot_imgs = tmp_rot_imgs.astype(np.float32) # Convert to float32 because LOMO uses opencv color conversion
            tmp_rot_imgs = tmp_rot_imgs.transpose(1,2,3,0)

            if LOMO_Feat:
                tmp_lomo = LOMOCT.LOMO(np.asarray(tmp_imgs))
                tmp_lomo_rot = LOMOCT.LOMO(np.asarray(tmp_rot_imgs))

                if LOMO_features is None:  
                    LOMO_features = tmp_lomo
                    LOMO_features_rotated = tmp_lomo_rot
                else:
                    LOMO_features = np.concatenate((LOMO_features, tmp_lomo),axis=0)
                    LOMO_features_rotated = np.concatenate((LOMO_features_rotated, tmp_lomo_rot),axis=0)

                    
            if LOMOSILTP_Feat:
                tmp_siltp = LOMOT.LOMO(np.asarray(tmp_imgs))
                tmp_siltp_rot = LOMOT.LOMO(np.asarray(tmp_rot_imgs))
                
                if LOMOSILTP_features is None:  
                    LOMOSILTP_features = tmp_siltp
                    LOMOSILTP_features_rotated = tmp_siltp_rot
                else:
                    LOMOSILTP_features = np.concatenate((LOMOSILTP_features, tmp_siltp),axis=0)
                    LOMOSILTP_features_rotated = np.concatenate((LOMOSILTP_features_rotated, tmp_siltp_rot),axis=0)

                
            if LOMOHSV_Feat:
                tmp_hsv = LOMOC.LOMO(np.asarray(tmp_imgs))
                tmp_hsv_rot = LOMOC.LOMO(np.asarray(tmp_rot_imgs))
                if LOMOHSV_features is None:  
                    LOMOHSV_features = tmp_hsv
                    LOMOHSV_features_rotated = tmp_hsv_rot
                else:
                    LOMOHSV_features = np.concatenate((LOMOHSV_features, tmp_hsv),axis=0)
                    LOMOHSV_features_rotated = np.concatenate((LOMOHSV_features_rotated, tmp_hsv_rot),axis=0)
                
                
            if ELF_Feat:
                tmp_elf = ELFCT.ELF(np.asarray(tmp_imgs))
                tmp_elf_rot = ELFCT.ELF(np.asarray(tmp_rot_imgs))

                if ELF_features is None:
                    ELF_features = tmp_elf
                    ELF_features_rotated = tmp_elf_rot
                else:
                    ELF_features = np.concatenate((ELF_features, tmp_elf),axis=0)
                    ELF_features_rotated = np.concatenate((ELF_features_rotated, tmp_elf_rot),axis=0)
                
            if ELFCOLOR_Feat:
                tmp_color = ELFC.ELF(np.asarray(tmp_imgs))
                tmp_color_rot = ELFC.ELF(np.asarray(tmp_rot_imgs))
                
                if ELFCOLOR_features is None:
                    ELFCOLOR_features = tmp_color
                    ELFCOLOR_features_rotated = tmp_color_rot
                else:
                    ELFCOLOR_features = np.concatenate((ELFCOLOR_features, tmp_color),axis=0)
                    ELFCOLOR_features_rotated = np.concatenate((ELFCOLOR_features_rotated, tmp_color_rot),axis=0)
                
            if ELFTEXTURE_Feat:
                tmp_texture = ELFT.ELF(np.asarray(tmp_imgs))
                tmp_texture_rot = ELFT.ELF(np.asarray(tmp_rot_imgs))
                
                if ELFTEXTURE_features is None:
                    ELFTEXTURE_features = tmp_texture
                    ELFTEXTURE_features_rotated = tmp_texture_rot
                else:
                    ELFTEXTURE_features = np.concatenate((ELFTEXTURE_features, tmp_texture),axis=0)
                    ELFTEXTURE_features_rotated = np.concatenate((ELFTEXTURE_features_rotated, tmp_texture_rot),axis=0)
            
            tmp_imgs = []
            tmp_rot_imgs = []
            


    ## Incase some of the last images didnt get features extracted, extract them
    if len(tmp_imgs) != 0 and (LOMO_Feat or LOMOSILTP_Feat or LOMOHSV_Feat or ELF_Feat or ELFCOLOR_Feat or ELFTEXTURE_Feat):
        n_bboxes = len(tmp_imgs)

        tmp_imgs = np.asarray(tmp_imgs)
        tmp_imgs = tmp_imgs.astype(np.float32) # Convert to float32 because LOMO uses opencv color conversion
        tmp_imgs = tmp_imgs.transpose(1,2,3,0)

        tmp_rot_imgs = np.asarray(tmp_rot_imgs)
        tmp_rot_imgs = tmp_rot_imgs.astype(np.float32) # Convert to float32 because LOMO uses opencv color conversion
        tmp_rot_imgs = tmp_rot_imgs.transpose(1,2,3,0)

    
        if LOMO_Feat:
            tmp_lomo = LOMOCT.LOMO(np.asarray(tmp_imgs))
            tmp_lomo_rot = LOMOCT.LOMO(np.asarray(tmp_rot_imgs))

            if LOMO_features is None:  
                LOMO_features = tmp_lomo
                LOMO_features_rotated = tmp_lomo_rot
            else:
                LOMO_features = np.concatenate((LOMO_features, tmp_lomo),axis=0)
                LOMO_features_rotated = np.concatenate((LOMO_features_rotated, tmp_lomo_rot),axis=0)
                
        if LOMOSILTP_Feat:
            tmp_siltp = LOMOT.LOMO(np.asarray(tmp_imgs))
            tmp_siltp_rot = LOMOT.LOMO(np.asarray(tmp_rot_imgs))
            
            if LOMOSILTP_features is None:  
                LOMOSILTP_features = tmp_siltp
                LOMOSILTP_features_rotated = tmp_siltp_rot
            else:
                LOMOSILTP_features = np.concatenate((LOMOSILTP_features, tmp_siltp),axis=0)
                LOMOSILTP_features_rotated = np.concatenate((LOMOSILTP_features_rotated, tmp_siltp_rot),axis=0)

        if LOMOHSV_Feat:
            tmp_hsv = LOMOC.LOMO(np.asarray(tmp_imgs))
            tmp_hsv_rot = LOMOC.LOMO(np.asarray(tmp_rot_imgs))
            if LOMOHSV_features is None:  
                LOMOHSV_features = tmp_hsv
                LOMOHSV_features_rotated = tmp_hsv_rot
            else:
                LOMOHSV_features = np.concatenate((LOMOHSV_features, tmp_hsv),axis=0)
                LOMOHSV_features_rotated = np.concatenate((LOMOHSV_features_rotated, tmp_hsv_rot),axis=0)
            
            
        if ELF_Feat:
            tmp_elf = ELFCT.ELF(np.asarray(tmp_imgs))
            tmp_elf_rot = ELFCT.ELF(np.asarray(tmp_rot_imgs))

            if ELF_features is None:
                ELF_features = tmp_elf
                ELF_features_rotated = tmp_elf_rot
            else:
                ELF_features = np.concatenate((ELF_features, tmp_elf),axis=0)
                ELF_features_rotated = np.concatenate((ELF_features_rotated, tmp_elf_rot),axis=0)

            
        if ELFCOLOR_Feat:
            tmp_color = ELFC.ELF(np.asarray(tmp_imgs))
            tmp_color_rot = ELFC.ELF(np.asarray(tmp_rot_imgs))
            
            if ELFCOLOR_features is None:
                ELFCOLOR_features = tmp_color
                ELFCOLOR_features_rotated = tmp_color_rot
            else:
                ELFCOLOR_features = np.concatenate((ELFCOLOR_features, tmp_color),axis=0)
                ELFCOLOR_features_rotated = np.concatenate((ELFCOLOR_features_rotated, tmp_color_rot),axis=0)
 
            
        if ELFTEXTURE_Feat:
            tmp_texture = ELFT.ELF(np.asarray(tmp_imgs))
            tmp_texture_rot = ELFT.ELF(np.asarray(tmp_rot_imgs))
            
            if ELFTEXTURE_features is None:
                ELFTEXTURE_features = tmp_texture
                ELFTEXTURE_features_rotated = tmp_texture_rot
            else:
                ELFTEXTURE_features = np.concatenate((ELFTEXTURE_features, tmp_texture),axis=0)
                ELFTEXTURE_features_rotated = np.concatenate((ELFTEXTURE_features_rotated, tmp_texture_rot),axis=0)
            


    # Save the extracted features
    labels = np.asarray(labels)
    
        
    LABELS_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_LABELS.pkl")
    FILENAMES_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_FILENAMES.pkl")
    ORIENTATION_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_ORIENTATION.pkl")
    OCCLUSION_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_OCCLUSION.pkl")
    GLITCH_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_GLITCH.pkl")
    TURNING_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_TURNING.pkl")
    
    LOMO_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_LOMO.pkl")       
    LOMOSILTP_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_LOMOSILTP.pkl")       
    LOMOHSV_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_LOMOHSV.pkl")       
    ELF_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_ELF.pkl")        
    ELFCOLOR_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_ELFCOLOR.pkl")        
    ELFTEXTURE_PKL = os.path.join(OUTPUT_DIR, DATASET + "_" + ORIENTATION + flipped + "_ELFTEXTURE.pkl")        
    
    with open(LABELS_PKL, "wb") as f:
        pickle.dump(labels, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(FILENAMES_PKL, "wb") as f:
        pickle.dump(filenames, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(ORIENTATION_PKL, "wb") as f:
        pickle.dump(orientation, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(OCCLUSION_PKL, "wb") as f:
        pickle.dump(orientation, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(GLITCH_PKL, "wb") as f:
        pickle.dump(orientation, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    with open(TURNING_PKL, "wb") as f:
        pickle.dump(orientation, f, protocol = pickle.HIGHEST_PROTOCOL)
        
    if LOMO_Feat:
        with open(LOMO_PKL, "wb") as f:
            pickle.dump(LOMO_features, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if LOMOSILTP_Feat:
        with open(LOMOSILTP_PKL, "wb") as f:
            pickle.dump(LOMOSILTP_features, f, protocol = pickle.HIGHEST_PROTOCOL)       
            
    if LOMOHSV_Feat:
        with open(LOMOHSV_PKL, "wb") as f:
            pickle.dump(LOMOHSV_features, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELF_Feat:
        with open(ELF_PKL, "wb") as f:
            pickle.dump(ELF_features, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELFCOLOR_Feat:
        with open(ELFCOLOR_PKL, "wb") as f:
            pickle.dump(ELFCOLOR_features, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELFTEXTURE_Feat:
        with open(ELFTEXTURE_PKL, "wb") as f:
            pickle.dump(ELFTEXTURE_features, f, protocol = pickle.HIGHEST_PROTOCOL)
    

    
    LOMO_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_LOMO.pkl")       
    LOMOSILTP_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_LOMOSILTP.pkl")       
    LOMOHSV_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_LOMOHSV.pkl")
    ELF_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_ELF.pkl")        
    ELFCOLOR_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_ELFCOLOR.pkl")        
    ELFTEXTURE_PKL = os.path.join(OUTPUT_DIR, DATASET + "_Rotated_" + ORIENTATION + flipped + "_ELFTEXTURE.pkl")          
        
    if LOMO_Feat:
        with open(LOMO_PKL, "wb") as f:
            pickle.dump(LOMO_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if LOMOSILTP_Feat:
        with open(LOMOSILTP_PKL, "wb") as f:
            pickle.dump(LOMOSILTP_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)       
            
    if LOMOHSV_Feat:
        with open(LOMOHSV_PKL, "wb") as f:
            pickle.dump(LOMOHSV_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELF_Feat:
        with open(ELF_PKL, "wb") as f:
            pickle.dump(ELF_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELFCOLOR_Feat:
        with open(ELFCOLOR_PKL, "wb") as f:
            pickle.dump(ELFCOLOR_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)
            
    if ELFTEXTURE_Feat:
        with open(ELFTEXTURE_PKL, "wb") as f:
            pickle.dump(ELFTEXTURE_features_rotated, f, protocol = pickle.HIGHEST_PROTOCOL)
    
    
    
def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
        
        
if __name__ == "__main__":
    
    ap = argparse.ArgumentParser(
        description = "Extracts the features on the supplied annotated data")
    ap.add_argument("-imageFolder", "--imageFolder", type=str, default = "../data/6Fish",
                help="Path to the folder with all the annotated images")
    ap.add_argument("-annotationFile", "--annotationFile", type=str, default = "../annotations/6F_2L_annotations_median_rotated.csv",
                help="Path to the .csv annotation file")
    ap.add_argument("-outputPath", "--outputPath", type=str, default = "../features",
                help="Path to the output folder where the extracted features are saved")
                
    ap.add_argument("-datasetName", "--datasetName", type=str, default = "6Fish",
                help="Root name used for all output files")

    ap.add_argument("-borderPad", "--borderPad", type=int, default = 5,
                help="Amount of border padding by side. Default is 5")
    ap.add_argument("-orientation", "--orientation", type=str, default = "both",
                help="The orientation of the fish used. Can be 'Both', 'Right' or 'Left'. Default is 'Both'")
    ap.add_argument("-flip", "--flip", type=str2bool, default = True,
                help="Whether the fish turned looking right should be vertically flipped, to look left instead. Default value is false")

    ap.add_argument("-ig", "--includeGlitches", action="store_true")
    ap.add_argument("-it", "--includeTurning", action="store_true")
    ap.add_argument("-io", "--includeOcclusions", action="store_true")
    
    ap.add_argument("-resizedWidth", "--resizedWidth", type=int, default = None,
                help="Width of the resized bounding boxes. If not provided it will be the median width of the boxes")
    ap.add_argument("-resizedHeight", "--resizedHeight", type=int, default = None,
                help="Height of the resized bounding boxes. If not provided it will be the median height of the boxes")
    ap.add_argument("-resizedWidthRotated", "--resizedWidthRotated", type=int, default = None,
                help="Width of the resized rotated bounding boxes. If not provided it will be the median width of the boxes")
    ap.add_argument("-resizedHeightRotated", "--resizedHeightRotated", type=int, default = None,
                help="Height of the resized rotated bounding boxes. If not provided it will be the median height of the boxes")
    
    args = vars(ap.parse_args())
    print(args)
    extract_features(args)