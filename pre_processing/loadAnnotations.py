# -*- coding: utf-8 -*-
"""
@author: Joakim Bruslund Haurum, Anastasija Karpova, Malte Pedersen, Stefan Hein Bengtson, and Thomas B. Moeslund
"""

import os
import cv2
import pandas as pd
import numpy as np
import csv
import argparse


def rotate(img, angle, center):
    """
    Rotates the input image by the given angle around the given center
    
    Input:
        img: Input image
        angle: Angle in degrees
        center: Tuple consisting of the center the image should be rotated around
        
    Output:
        dst: The rotated image
    """
    
    rows, cols = img.shape[:2]
    
    M = cv2.getRotationMatrix2D(center,angle,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    return dst
    

def findRotation(img):
    """
    Calculates the rotation of the foreground pixels in the binary image
    
    Input:
        img: Binary input image, 
        
    Output:
        theta : The orientation in degrees from [-pi/2 : pi/2]
    """

    y, x = np.nonzero(img)

    cov = np.cov(np.vstack([x,y]))
    
    ## Get the eigenvalues/vectors and sort them by descending eigenvalues
    U, S, _ = np.linalg.svd(cov)
    x_v1, y_v1 = U[:,0]
    
    theta = np.arctan((y_v1)/(x_v1))   # arctan vs arctan2. Use arctan2 to handle x_v1 = 0?
    
    return np.rad2deg(theta)
    


def rotate_annotations(args):
    """
    Calculates the rotation of the annotations in an input annotation file, and saves it in a new annotation file
    
    Input:
        args: A dictionary, holding the fields ['imageFolder', 'annotationFile', 'outputFile' and 'backgroundFile']
    
    Output:
        A new csv file of the annotations, with the orientation of the fish, and the bounding box of the fish in the rotated image
    """
    
    IMAGE_FOLDER = args["imageFolder"]
    annotations_path = os.path.join("..", "annotations")
    ANNOTATION_FILE = os.path.join(annotations_path, args["annotationFile"])
    OUTPUT_FILE = os.path.join(annotations_path, args["outputFile"])
    BACKGROUND_IMAGE = args["backgroundFile"]
    
    # Load the annotation file
    fish_dataframe = pd.read_csv(ANNOTATION_FILE, sep=";")
	
    valid_images = [".jpg",".gif",".png",".tga"]
    
    # Check if a background image is provided. If not, then calculate a median image over 100 frames
    if BACKGROUND_IMAGE == "":
        background_available = False
    else:
        background_available = True
        
    background = None
    
    if background_available:
        background = cv2.imread(BACKGROUND_IMAGE,1)
        background = cv2.cvtColor(background, cv2.COLOR_BGR2RGB)
    else:
        np.random.seed(0)
        n_medianImages = 100
        medianImg_lst = []
        
        filenames = os.listdir(IMAGE_FOLDER)
        image_filenames = [x for x in filenames if os.path.splitext(x)[1].lower() in valid_images]
        image_filenames = np.asarray(image_filenames)

        median_files = image_filenames[np.random.choice(len(image_filenames), n_medianImages, replace=False)]		
        for file in median_files:    
            print(file)
            img = cv2.imread(os.path.join(IMAGE_FOLDER, file),1)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            medianImg_lst.append(img)
            
        medianImgs = np.asarray(medianImg_lst)
        background = np.median(medianImgs, axis=0)
        background = background.astype(dtype=np.uint8)
        
        cv2.imwrite(args["annotationFile"].split(".")[0] + "_median.png", cv2.cvtColor(background, cv2.COLOR_RGB2BGR))
        
        del medianImgs, medianImg_lst, filenames, image_filenames, median_files    
    
    
    # A 7x/ circular SE is used for the morphological operations
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,((7,7)))
            
    with open(OUTPUT_FILE, 'w', newline = "") as csvWriteFile:
        writer = csv.writer(csvWriteFile, delimiter=";")
        
        # Write the headers in the new csv file
        firstrow = []
        firstrow.append("Filename")
        firstrow.append("Object ID")
        firstrow.append("Annotation tag")
        firstrow.append("Upper left corner X")
        firstrow.append("Upper left corner Y")
        firstrow.append("Lower right corner X")
        firstrow.append("Lower right corner Y")
        firstrow.append("Right")
        firstrow.append("Turning")
        firstrow.append("Occlusion")
        firstrow.append("Glitch")
        firstrow.append("Angle")
        firstrow.append("Rotation center X")
        firstrow.append("Rotation center Y")
        firstrow.append("Rotation Upper left corner X")
        firstrow.append("Rotation Upper left corner Y")
        firstrow.append("Rotation Lower right corner X")
        firstrow.append("Rotation Lower right corner Y")
         
        writer.writerow(firstrow)
        
        
        for file in os.listdir(IMAGE_FOLDER):
            ext = os.path.splitext(file)[1]
            if ext.lower() not in valid_images:
                continue
            
            # Load the annotations for the current frame
            annotations = fish_dataframe[fish_dataframe["Filename"] == file]
            
            if annotations.shape[0] == 0:
                continue
                
            # Get the properties of the frame
            tags = annotations["Annotation tag"].values
            IDs = annotations["Object ID"].values
            
            ULX = annotations["Upper left corner X"].values
            ULY = annotations["Upper left corner Y"].values
            LRX = annotations["Lower right corner X"].values
            LRY = annotations["Lower right corner Y"].values

            # Load the frame
            img = cv2.imread(os.path.join(IMAGE_FOLDER, file), 1)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            
            # Subtract the iamge fro mthe background img, convert to grayscale, and thresholded with a value of 3 (found through trial and error)
            img_sub_diff = cv2.absdiff(img, background)
            img_sub_col = np.mean(img_sub_diff, axis=2, dtype=np.uint8)
            _, img_sub = cv2.threshold(img_sub_col, 3, 255, cv2.THRESH_BINARY)
            del img_sub_diff, img_sub_col
            
            # Go through the IDs in the image
            for idx in range(len(IDs)):
                
                
                ## Get the bounding box area of the input, and apply morphological opening and closing to remove noise
                img_sub_crop = img_sub[ULY[idx]:LRY[idx], ULX[idx]:LRX[idx]]
                img_sub_crop = cv2.morphologyEx(img_sub_crop, cv2.MORPH_OPEN, kernel)
                img_sub_crop = cv2.morphologyEx(img_sub_crop, cv2.MORPH_CLOSE, kernel)
                
                # Find the largest blob in the bounding box
                _, contours, hierarchy = cv2.findContours(img_sub_crop,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
                
                mask = np.zeros(img_sub_crop.shape[:2], dtype="uint8")
                maxAreaContour = None
                
                # If only one blob in the image, just use that, otherwise find the largest
                # This is done in case a bit of a second fish appears in the image
                if len(contours) == 1:
                    maxAreaContour = contours[0]
                else:  
                    maxArea = -np.infty
                    for c in contours:
                        area = cv2.contourArea(c)
                        if area > maxArea:
                            maxAreaContour = c
                            maxArea = area
                        
                # Draw the found largest contour onto a binary mask, and bitwise 
                # and it with the bounding box image, keeping only the largest BLOB
                cv2.drawContours(mask, [maxAreaContour], -1, 255, -1)
                img_sub_crop = cv2.bitwise_and(img_sub_crop, img_sub_crop, mask = mask)
                
                # Place the found BLOB into a large empty canvas, same size as the input image
                full_mask = np.zeros(img_sub.shape[:2], dtype="uint8")
                full_mask[ULY[idx]:LRY[idx], ULX[idx]:LRX[idx]] = mask
                
                
                # Find the center of the largest BLOB
                y, x = np.nonzero(full_mask)
                x_center = np.mean(x)
                y_center = np.mean(y)
                
                del x, y
                
                ## Rotate the mask, and find the corners of the closest fitting bounding box
                theta_deg = findRotation(img_sub_crop)
                rotated_sub = rotate(full_mask, theta_deg, center = (x_center, y_center))

                
                
                y, x = np.nonzero(rotated_sub)
                
                x_max = np.max(x)
                x_min = np.min(x)
                y_max = np.max(y)
                y_min = np.min(y)
                del x,y

                metadata = annotations["Right,Turning,Occlusion,Glitch"].values[idx]
                metadata = metadata.split(",")

                
                #Write everything into the new .csv file
                row = []
                row.append(file)
                row.append(IDs[idx])
                row.append(tags[idx])
                row.append(ULX[idx])
                row.append(ULY[idx])
                row.append(LRX[idx])
                row.append(LRY[idx])
                row.append(int(metadata[0]))
                row.append(int(metadata[1]))
                row.append(int(metadata[2]))
                row.append(int(metadata[3]))
                row.append(theta_deg)
                row.append(x_center)
                row.append(y_center)
                row.append(x_min)
                row.append(y_min)
                row.append(x_max)
                row.append(y_max)
                 
                writer.writerow(row)
        
        
if __name__ == "__main__":
    
    ap = argparse.ArgumentParser(
        description = "Takes an annotation file and calculates the median image of the image series, and an annotation file for the rotated fish annotations")
    ap.add_argument("-imageFolder", "--imageFolder", type=str, default = "../data/6Fish",
                help="Path to the folder with all the annotated images")
    ap.add_argument("-annotationFile", "--annotationFile", type=str, default = "6F_2L_annotations.csv",
                help="Path to the .csv annotation file")
    ap.add_argument("-outputFile", "--outputFile", type=str, default = "6F_2L_annotations_median_rotated.csv",
                help="Name of the output .csv annotation file")
    ap.add_argument("-backgroundFile", "--backgroundFile", type=str, default = "",
                help="Path to the background image. If no background image, a median image is calculated and used")
    
    args = vars(ap.parse_args())
    rotate_annotations(args)