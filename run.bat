
cd pre_processing
python loadAnnotations.py
python extractFeatures.py

cd ..

cd evaluation

python evaluateFeaturesPCATest.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELF.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELF -nTrain 100 -outputPath REPO_PATH\results/ELF_Rotated_PCA
python evaluateFeaturesPCATest.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMO.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMO -nTrain 100 -outputPath REPO_PATH\results/LOMO_Rotated_PCA


python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELF.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELF -pcaRatio 0.95 -nTrain 100 -outputPath REPO_PATH\results/ELF_Rotated_VarTrain
python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELFCOLOR.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELFCOLOR -pcaRatio 0.95 -nTrain 100 -outputPath REPO_PATH\results/ELFCOLOR_Rotated_VarTrain
python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELFTEXTURE.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELFTEXTURE -pcaRatio 0.95 -nTrain 100 -outputPath REPO_PATH\results/ELFTEXTURE_Rotated_VarTrain

python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMO.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMO -pcaRatio 0.60 -nTrain 100 -outputPath REPO_PATH\results/LOMO_Rotated_VarTrain
python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMOHSV.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMOHSV -pcaRatio 0.60 -nTrain 100 -outputPath REPO_PATH\results/LOMOHSV_Rotated_VarTrain
python evaluateFeaturesVarTrain.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMOSILTP.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMOSILTP -pcaRatio 0.60 -nTrain 100 -outputPath REPO_PATH\results/LOMOSILTP_Rotated_VarTrain




python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELF.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELF -pcaRatio 0.95 -outputPath REPO_PATH\results/ELF_Rotated
python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELFCOLOR.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELFCOLOR -pcaRatio 0.95 -outputPath REPO_PATH\results/ELFCOLOR_Rotated
python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_ELFTEXTURE.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_ELFTEXTURE -pcaRatio 0.95 -outputPath REPO_PATH\results/ELFTEXTURE_Rotated

python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMO.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMO -pcaRatio 0.60 -outputPath REPO_PATH\results/LOMO_Rotated
python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMOHSV.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMOHSV -pcaRatio 0.60 -outputPath REPO_PATH\results/LOMOHSV_Rotated
python evaluateFeatures.py -directoryPath "REPO_PATH\features" -trainFeaturesPath 6Fish_Rotated_both_Flipped_LOMOSILTP.pkl -trainLabelsPath 6Fish_both_Flipped_LABELS.pkl -trainFilesPath 6Fish_both_Flipped_FILENAMES.pkl -rootOutputName 6Fish_Rotated_both_Flipped_LOMOSILTP -pcaRatio 0.60 -outputPath REPO_PATH\results/LOMOSILTP_Rotated

cd ..
